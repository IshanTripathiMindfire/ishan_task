<!-- Including the master layout to create login page -->

@extends('layouts.app')

<!-- Adding title to the page -->

@section('title', 'Login Page')

@section('content')

    <div class = 'container' id = 'table_container'>
        {{ csrf_field() }}
        <div>{{ Html::link('logout', 'Logout', ['class' => 'form-control btn-primary form-control text-center']) }}</div>
        <div class = 'table-responsive display'>
            <table class = 'table-bordered table-hover' id = 'data-table'>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Residential Address</th>
                        <th>Designation</th>
                        <th>Company</th>
                        <th>Company Address</th>
                        <th>Telephone</th>
                        <th>Mobile</th>
                        <th>Email</th>
                        <th>Stack Overflow Id</th>
                    </tr>
                </thead>
                <tbody>

                @foreach ($data as $values)
                    <tr id= {{ $values -> PK_id }}>
                        <td>{{ $values -> name }}</td>
                        <td>{{ $values -> residential_address }}</td>
                        <td>{{ $values -> designation }}</td>
                        <td>{{ $values -> company }}</td>
                        <td>{{ $values -> official_address }}</td>
                        <td>{{ $values -> telephone }}</td>
                        <td>{{ $values -> mobile }}</td>
                        <td>{{ $values -> email }}</td>
                        <td>{{ $values -> stackoverflow_id }}</td>
                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div id="modal" class="modal fade " role="dialog">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div id="loader" class="loader hidden "></div>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Employee Details</h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id = "update" >Update</button>
                    <button type="button" class="btn btn-default" id = "delete" >Delete</button>
                    <button type="button" class="btn btn-default" id = "stack" >Stack Overflow</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <div id="modal_update" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="update_model" >
                    <div class="modal-header form-group">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Update Employee Details</h4>
                    </div>
                    <div class="modal-body form-group" id="modal-body-update">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors -> all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <div class="modal-footer form-group">
                        <button type="button" class="btn btn-default" id="update_db" >Update</button>
                        <button type="button" class="btn btn-default" id = "close_update" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="modal_delete" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form>
                    {{ csrf_field() }}
                    <div class="modal-header form-group">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Delete Employee Details</h4>
                    </div>
                    <div class="modal-body form-group" id="modal-body-delete">
                    </div>
                    <div class="modal-footer form-group">
                        <button type="submit" class="btn btn-default" id="delete_db" >Delete</button>
                        <button type="button" class="btn btn-default" id="close_delete" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="modal_stack" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form >
                    <div class="modal-header form-group">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Stack Overflow Employee Details</h4>
                    </div>
                    <div class="modal-body form-group" id="modal-body-stackoverflow">
                    </div>
                    <div class="modal-footer form-group">
                        <button type="button" class="btn btn-default" id="close_stack" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection