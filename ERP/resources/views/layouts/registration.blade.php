<!-- Including the master layout to create login page -->

@extends('layouts.app')

<!-- Adding title to the page -->

@section('title', 'Login Page')

@section('content')

    <div class="container form-group">

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {{ Form::open(array('url' => 'register','method' => 'post','id'=>'registration_form')) }}
            <fieldset>
                <div class="row">
                    <legend>Personal Details</legend>
                </div>
                <div class="row">
                    <div class="col-lg-1 col-xs-3">
                        <span><label class="control-label">Prefix:</label></span>
                    </div>

                    <div class="col-lg-1  col-xs-2">
                        <span><label for="prefix_mr">
                            {{ Form::radio('prefix', 'Mr.',null, ['id' => 'prefix_mr', 'checked' => 'checked']) }}
                            <span>Mr.</span>
                        </label></span>
                    </div>

                    <div class="col-lg-1 col-xs-2">
                        <span><label for="prefix_mrs">
                            {{ Form::radio('prefix', 'Mrs.',null, ['id' => 'prefix_mrs']) }}
                            <span>Mrs.</span>
                        </label></span>
                    </div>

                    <div class="col-lg-1 col-xs-2">
                        <span><label for="prefix_ms">
                            {{ Form::radio('prefix', 'Ms.',null, ['id' => 'prefix_ms']) }}
                            <span>Ms.</span>
                        </label></span>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <span><label>First Name:</label></span>
                        {{ Form::text('first_name', null, ['class' => 'form-control', 'placeholder' => 'Enter the first name here']) }}
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <span><label>Middle Name:</label></span>
                        {{ Form::text('middle_name', null, ['class' => 'form-control', 'placeholder' => 'Enter the middle name here']) }}
                    </div>

                    <div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
                        <span><label>Last Name:</label></span>
                        {{ Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => 'Enter the last name here']) }}
                    </div>
                </div>

                <div class="row" id="registration_gender">
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-5">
                        <span><label class="control-label">Gender:</label></span>
                    </div>

                    <div class="col-lg-offset-1 col-lg-1 col-md-2 col-sm-3 col-xs-7">
                        <span><label>
                            {{ Form::select('gender', ['Male' => 'Male', 'Female' => 'Female'], null) }}
                        </label></span>
                    </div>

                    <div class="col-lg-offset-2 col-lg-2 col-md-2 col-sm-3 col-xs-3 ">
                        <span><label class="control-label">Marital Status:</label></span>
                    </div>

                    <div class="col-lg-1 col-md-2 col-sm-4 col-xs-2 col-xs-offset-1">
                        <span><label>
                            {{ Form::select('marital_status', ['Married' => 'Married', 'Single' => 'Single', 'Divorce' => 'Divorce'], null) }}
                        </label></span>
                    </div>
                </div>

                <div class="row" id="dob">
                    <div class="col-lg-1 col-md-2 col-sm-3 col-xs-4">
                        <span><label>Date of Birth:</label></span>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-2 col-xs-6 col-xs-offset-1">
                       {{ Form::date('date_of_birth', \Carbon\Carbon::now()) }}
                    </div>

                    <div class="col-lg-offset-1 col-lg-2 col-md-2 col-sm-3 col-xs-12">
                        <span><label>Stack Overflow Id:</label></span>
                    </div>
                    <div class="col-lg-offset-1 col-lg-4 col-md-3 col-sm-2 col-xs-12">
                        {{ Form::text('stack_overflow_id', null, ['class' => 'form-control', 'placeholder' => 'Enter the stack overflow id here']) }}
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="row">
                    <legend>Communication Profile </legend>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <span><label>Telephone:</label></span>
                        {{ Form::text('telephone', null, ['class' => 'form-control', 'placeholder' => 'Enter the telephone number here']) }}
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <span><label>Mobile:</label></span>
                        {{ Form::text('mobile', null, ['class' => 'form-control', 'placeholder' => 'Enter the mobile number here']) }}
                    </div>
                </div>

                <div class="row">
                    <div class="cl-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <span><label>Email:</label></span>
                        {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Enter the Email id here']) }}
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <span><label>Fax:</label></span>
                        {{ Form::text('fax', null, ['class' => 'form-control', 'placeholder' => 'Enter the fax number here']) }}
                    </div>
                </div>

                <div class="row">
                    <div class="cl-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <span><label>Password:</label></span>
                        {{ Form::password('password', ['class' => 'form-control', 'placeholder' => 'Enter the Password here', 'id' => 'password']) }}
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <span><label>Confirm Password:</label></span>
                        {{ Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Enter the confirm password here']) }}
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <span><label>Residential Address :</label></span>
                        {{ Form::textarea('residential_address', null, ['class' => 'form-control', 'placeholder' => 'Enter the residential address here']) }}
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <span><label>Residential City :</label></span>
                        {{ Form::text('residential_city', null, ['class' => 'form-control', 'placeholder' => 'Enter the residential city here']) }}
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <span><label>Residential State :</label></span>
                        {{ Form::text('residential_state', null, ['class' => 'form-control', 'placeholder' => 'Enter the residential state here']) }}
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <span><label>Residential Zip Code :</label></span>
                        {{ Form::text('residential_zip_code', null, ['class' => 'form-control', 'placeholder' => 'Enter the residential zip code  here']) }}
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="row">
                    <legend>Corporate Profile </legend>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <span><label>Designation:</label></span>
                            {{ Form::text('designation', null, ['class' => 'form-control', 'placeholder' => 'Enter the designation here']) }}
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <span><label>Company:</label></span>
                        {{ Form::text('company', null, ['class' => 'form-control', 'placeholder' => 'Enter the company name here']) }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <span><label>Official Address :</label></span>
                        {{ Form::textarea('official_address', null, ['class' => 'form-control', 'placeholder' => 'Enter the official address  here']) }}
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <span><label>Official City :</label></span>
                        {{ Form::text('official_city', null, ['class' => 'form-control', 'placeholder' => 'Enter the official city here']) }}
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <span><label>Official State :</label></span>
                        {{ Form::text('official_state', null, ['class' => 'form-control', 'placeholder' => 'Enter the official state here']) }}
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <span><label>Official Zip Code :</label></span>
                        {{ Form::text('official_zip_code', null, ['class' => 'form-control', 'placeholder' => 'Enter the official zip code here']) }}
                    </div>
                </div>
                <div class="row" id="comm_medium">
                    <div class="col-lg-3 col-xs-12">
                        <span><label class="control-label">Communication medium:</label></span>
                    </div>

                    <div class="col-lg-1  col-xs-2">
                        <span><label for="sms">
                            {{Form::checkbox('communication_medium[]', 'SMS', null, ['id' => 'sms','checked'])}}
                            <span>SMS</span>
                        </label></span>
                    </div>

                    <div class="col-lg-1  col-xs-2">
                        <span><label for="call">
                            {{Form::checkbox('communication_medium[]', 'Call', null, ['id' => 'call','checked'])}}
                            <span>Call</span>
                        </label></span>
                    </div>

                    <div class="col-lg-2  col-xs-2">
                        <span><label for="email">
                            {{Form::checkbox('communication_medium[]', 'Email', null, ['id' => 'email','checked'])}}
                            <span>Email</span>
                        </label></span>
                    </div>
                </div>
                <div class="row" id="register_btn">
                    <div class="col-lg-4">
                        {{ Form::submit('Register!', ['class' => 'form-control btn-primary form-control','id' => 'registerbtn']) }}
                    </div>
                    <div class="col-lg-4">
                        {{ Html::link('/', 'Back!', ['class' => 'form-control btn-primary form-control text-center']) }}
                    </div>
                </div>
            </fieldset>
        {{ Form::close() }}
    </div>
@endsection
