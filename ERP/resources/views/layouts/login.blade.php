<!-- Including the master layout to create login page -->

@extends('layouts.app')

<!-- Adding title to the page -->

@section('title', 'Login Page')

@section('content')

    <div class="container">

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors -> all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="col-lg-offset-3 col-md-offset-2 col-lg-6 col-md-8 col-sm-12 form-group" id="login_box">
            {{ Form::open(array('url' => 'dashboard','method' => 'post')) }}
                <fieldset>
                    <div class="row" id="login_label"><legend><label class="control-label">Login</label></legend></div>
                    <div class="row" id="login_email">
                        <div class="col-lg-3"><label>Email Id:</label></div>
                        <div class="col-lg-9">
                           {{ Form::email('login_email_input', null , ['class' => 'form-control', 'placeholder' => 'abc@xyz.com']) }}
                        </div>
                    </div>

                    <div class="row" id="login_password">
                        <div class="col-lg-3"><label class="control-label">Password:</label></div>
                        <div class="col-lg-9">
                            {{ Form::password('login_password_input', ['class' => 'form-control', 'placeholder' => 'pass']) }}
                        </div>
                    </div>

                    <div class="row" id="login_btn">
                        <div class="col-lg-offset-1 col-lg-4">
                            {{ Form::submit('Go!', ['class' => 'form-control btn-primary form-control']) }}
                        </div>

                        <div class="col-lg-offset-2 col-lg-4">
                            {{ Html::link('registration', 'Sign Up!', ['class' => 'form-control btn-primary form-control text-center']) }}
                        </div>
                    </div>
                </fieldset>
            {{ Form::close() }}
        </div>
    </div>

@endsection
