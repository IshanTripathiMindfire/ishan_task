$('document').ready(function () {
    jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z]+$/i.test(value);
    }, "Please enter only letters.");
    $('#registration_form').validate({
       rules: {
           prefix : "required",

           first_name : {
               required : true,
               lettersonly : true

           },

           middle_name : {
               lettersonly : true
           },

           last_name : {
             required : true,
             lettersonly : true
           },

           gender : 'required',

           marital_status : 'required',

           date_of_birth : {
               required : true,
               date :  true
           },

           stack_overflow_id : 'digits',

           telephone : {
             required : true,
             digits : true,
             maxlength : 10,
             minlength : 10
           },

           mobile : {
               required : true,
               digits : true,
               minlength : 10,
               maxlength : 10
           },

           email : {
               required : true,
               email : true
           },

           fax : {
               digits : true,
               maxlength : 10,
               minlength : 10
           },

           password : {
             required : true
           },

           password_confirmation : {
               required : true,
               equalTo : '#password'
           },

           residential_address : {
               required : true
           },

           residential_city : {
             required :true,
             lettersonly : true
           },

           residential_state : {
               required : true,
               lettersonly : true
           },

           residential_zip_code : {
               required : true,
               digits : true,
               minlength : 6,
               maxlength : 10
           },

           official_address : {
               required : true
           },

           official_city : {
               required :true,
               lettersonly : true
           },

           official_state : {
               required : true,
               lettersonly : true
           },

           official_zip_code : {
               required : true,
               digits : true,
               minlength : 6,
               maxlength : 10
           },

           designation : {
               required : true,
               lettersonly : true
           },

           company : {
               required : true,
               lettersonly : true
           },

       },
       messages : {
           password_confirmation : {
               equalTo : 'Password mismatch'
           }
       },
        highlight: function (element) {
            $(element).parent().addClass('error')
        },
        unhighlight: function (element) {
            $(element).parent().removeClass('error')
        }
   });
});

$(document).ready(function() {
    $('#data-table').DataTable();
});

$(document).ready(function() {
    $('#data-table').on('click', 'tr', function() {
        var id_request = $(this).attr('id');
        $("#update").data("ids",id_request);
        $("#delete").data("ids",id_request);
        $("#stack").data("ids",id_request);
        $.ajax( {
            url  :"modal_data/"+id_request,
            type : "GET",
            success : function (response) {
                $("#modal").modal("show");
                $("#modal .modal-body ").html("<div><b>Name : </b>"+response[0].name+"</div>"+
                "<div><b>Residential Address : </b>"+response[0].residential_address+"</div>"+
                "<div><b>Office Address : </b>"+response[0].official_address+"</div>"+
                "<div><b>Telephone : </b>"+response[0].telephone+"</div>"+
                "<div><b>Mobile : </b>"+response[0].mobile+"</div>"+
                "<div><b>Email : </b>"+response[0].email+"</div>"+
                "<div><b>Designation : </b>"+response[0].designation+"</div>"+
                "<div><b>Company : </b>"+response[0].company+"</div>"+
                "<div><b>Stack overflow id : </b>"+response[0].stackoverflow_id+"</div>");
            }
        });
    });
});


$("#modal_update").ready(function() {
    $("#update").click(function() {
        var id_request = $(this).data('ids');
        $("#update_db").data("ids",id_request);
        $.ajax( {
            url  : "modal_data_update/"+id_request,
            type : "get",
            success : function (response) {
                $("#modal").modal("hide")
                $("#modal_update").modal("show");
                $("#modal-body-update").attr("height",$("#modal-body-update").height($(window).height()-250));
                $("#modal_update .modal-body").html(
                    "<div><input type='hidden' class='form-control' name='update_id' id='update_id' value='"+response[0].PK_id+"' ></div>"+
                    "<div class='form-group'><b>Prefix : </b><input type='text' class='form-control' name='update_prefix' id='update_prefix' value='"+response[0].prefix+"' disabled='true'></div>"+
                    "<div class='form-group'><b>First Name : </b><input type='text' class='form-control' name='update_first_name' id='update_first_name' value='"+response[0].first_name+"' disabled='true'></div>"+
                    "<div class='form-group'><b>Middle Name : </b><input type='text' class='form-control' name='update_middle_name' id='update_middle_name' value='"+response[0].middle_name+"' disabled='true'></div>"+
                    "<div class='form-group'><b>Last Name : </b><input type='text' class='form-control' name='update_last_name' id='update_last_name' value='"+response[0].last_name+"' disabled='true'></div>"+
                    "<div class='form-group'><b>Date of birth : </b><input type='date' class='form-control' name='update_dob' id='update_dob' value='"+response[0].date_of_birth+"'></div>"+
                    "<div class='form-group'><b>Residential Street : </b><input type='text' class='form-control' name='update_r_street' id='update_r_street' value='"+response[0].r_street+"'></div>"+
                    "<div class='form-group'><b>Residential City : </b><input type='text' class='form-control' name='update_r_city' id='update_r_city' value='"+response[0].r_city+"'></div>"+
                    "<div class='form-group'><b>Residential State : </b><input type='text' class='form-control' name='update_r_state' id='update_r_state' value='"+response[0].r_state+"'></div>"+
                    "<div class='form-group'><b>Residential Zip Code : </b><input type='text' class='form-control' name='update_r_zip_code' id='update_r_zip_code' value='"+response[0].r_zip+"'></div>"+
                    "<div class='form-group'><b>Official Street : </b><input type='text' class='form-control' name='update_o_street' id='update_o_street' value='"+response[0].o_street+"'></div>"+
                    "<div class='form-group'><b>Official City : </b><input type='text' class='form-control' name='update_o_city' id='update_o_city' value='"+response[0].o_city+"'></div>"+
                    "<div class='form-group'><b>Official State : </b><input type='text' class='form-control' name='update_o_state' id='update_o_state' value='"+response[0].o_state+"'></div>"+
                    "<div class='form-group'><b>Official Zip Code : </b><input type='text' class='form-control' name='update_o_zip_code' id='update_o_zip_code' value='"+response[0].o_zip+"'></div>"+
                    "<div class='form-group'><b>Telephone : </b><input type='text' class='form-control' name='update_telephone' id='update_telephone' value='"+response[0].telephone+"'></div>"+
                    "<div class='form-group'><b>Mobile : </b><input type='text' class='form-control' name='update_mobile' id='update_mobile' value='"+response[0].mobile+"'></div>"+
                    "<div class='form-group'><b>Email : </b><input type='text' class='form-control' name='update_email' id='update_email' value='"+response[0].email+"' disabled></div>"+
                    "<div class='form-group'><b>Password : </b><input type='text' class='form-control' name='update_password' id='update_password' value='"+response[0].password+"'></div>"+
                    "<div class='form-group'><b>Designation : </b><input type='text' class='form-control' name='update_designation' id='update_designation' value='"+response[0].designation+"'></div>"+
                    "<div class='form-group'><b>Company : </b><input type='text' class='form-control' name='update_company' id='update_company' value='"+response[0].company+"'></div>"+
                    "<div class='form-group'><b>Stack Overflow id : </b><input type='text' class='form-control' name='update_stackoverflow_id' id='update_stackoverflow_id' value='"+response[0].stackoverflow_id+"'></div>");
            }
        });
    });
});

$("#modal_update").ready(function() {
    $("#update_db").click(function () {

        $(".text-danger").remove();
        var id_request = $(this).data('ids');
        $('#update_model').validate({

            rules: {

                update_dob : {
                    required : true,
                    date :  true

                },

                update_stackoverflow_id : 'digits',

                update_telephone : {
                    required : true,
                    digits : true,
                    maxlength : 10,
                    minlength : 10
                },

                update_mobile : {
                    required : true,
                    digits : true,
                    minlength : 10,
                    maxlength : 10
                },

                update_r_street : {
                    required : true
                },

                update_r_city : {
                    required :true,
                    lettersonly : true
                },

                update_r_state : {
                    required : true,
                    lettersonly : true
                },

                update_r_zip_code : {
                    required : true,
                    digits : true,
                    minlength : 6,
                    maxlength : 10
                },

                update_o_street : {
                    required : true
                },

                update_o_city : {
                    required :true,
                    lettersonly : true
                },

                update_o_state : {
                    required : true,
                    lettersonly : true
                },

                update_o_zip_code : {
                    required : true,
                    digits : true,
                    minlength : 6,
                    maxlength : 10
                },

                update_designation : {
                    required : true,
                    lettersonly : true
                },

                update_company : {
                    required : true,
                    lettersonly : true
                },

            },
            highlight: function (element) {
                $(element).parent().addClass('error')
            },
            unhighlight: function (element) {
                $(element).parent().removeClass('error')
            }
        });


        $.ajax( {
            url  : "update_db",
            type : "POST",
            data : $("form").serialize(),
            success : function (response) {
                if(response == 'true') {
                    $("#modal_update").modal("hide");
                    $("#" + id_request).html("<td>" + $("#update_prefix").val() + " " + $("#update_first_name").val() + " " + $("#update_middle_name").val() + " " + $("#update_last_name").val() + "</td>" +
                    "<td>" + $("#update_r_street").val() + " " + $("#update_r_city").val() + " " + $("#update_r_state").val() + " " + $("#update_r_zip_code").val() + "</td>" +
                    "<td>" + $("#update_telephone").val() + "</td>" +
                    "<td>" + $("#update_mobile").val() + "</td>" +
                    "<td>" + $("#update_email").val() + "</td>" +
                    "<td>" + $("#update_designation").val() + "</td>" +
                    "<td>" + $("#update_company").val() + "</td>" +
                    "<td>" + $("#update_o_street").val() + " " + $("#update_o_city").val() + " " + $("#update_o_state").val() + " " +
                    $("#update_o_zip_code").val() + "</td> " +
                    "<td>"+$("#update_stackoverflow_id").val() + "</td>");
                }
                else
                {
                    $("#modal_update .modal-body").html("<div> Update Error </div>");
                }
            }
        });

    });
});

$("#modal_delete").ready(function() {
    $("#delete").click(function() {
        var  id_request = $(this).data('ids');
        $("#delete_db").data("ids",id_request);
        $("#modal_delete .modal-body").html("Are you sure you want to delete it");
        $("#modal").modal("hide");
        $("#modal_delete").modal("show");
    });
});

$("#modal_delete").ready(function() {
    $("#delete_db").click(function() {
        var id_request = $(this).data('ids');
        $.ajax( {
            url  : "delete_data/"+id_request,
            type : "GET",
            success : function (response) {
                console.log(response);
            }
        });
        $("#"+id_request).remove();
        $("#modal_db").modal("hide");
    });
    $("#close_delete").click(function() {
        $("#modal_delete").modal("hide");
        $("#modal").modal("show");
    });
});

$("#modal_stack").ready(function() {
    $("#stack").click(function() {
        var id_request = $(this).data('ids');
        $("#loader").removeClass("hidden");
        var stack_id = $('#'+id_request+' td:last').text();
        console.log(stack_id);
        $("#modal .modal-body, #modal .modal-header, #modal .modal-footer").addClass("hidden");
        $.ajax({
            url  : "https://api.stackexchange.com/2.2/users/"+stack_id+"?order=desc&sort=reputation&site=stackoverflow",
            type : "GET",
            dataType : "json",
            timeout : 10000,
            success : function (response_stack) {
                if(response_stack.items != "") {
                    $("#modal").modal("hide");
                    $("#modal_stack").modal("show");
                    $("#modal-body-stackoverflow").attr("height",$("#modal-body-stackoverflow").height($(window).height()-250));
                    $("#modal_stack .modal-body ").html("<div class='form-group'><div ><center><img src='"+response_stack.items[0].profile_image+"' " +
                    "class='img-circle img-responsive' alt='No image'></center></div>" +
                    "<div class='col-lg-12'><span><label>Website url:</label></span><span>" +
                    "<input type='text' class='form-control' value='"+response_stack.items[0].website_url+"' " +
                    "disabled></span></div>" +
                    "<div class='col-lg-12'><span><label>Link:</label></span><span>"+
                    "<input type='text' class='form-control' value='"+response_stack.items[0].link+"' disabled></div>" +
                    "<div class='col-lg-12'><span><label>Display Name:</label></span><span>"+
                    "<input type='text' class='form-control' value='"+response_stack.items[0].display_name+"' disabled></div>" +
                    "<div class='col-lg-4'><span><label>Badge Bronze:</label></span><span>"+
                    "<input type='text' class='form-control' value='"+response_stack.items[0].badge_counts.bronze+"' disabled></div>" +
                    "<div class='col-lg-4'><span><label>Badge Silver:</label></span><span>"+
                    "<input type='text' class='form-control' value='"+response_stack.items[0].badge_counts.silver+"' disabled></div>" +
                    "<div class='col-lg-4'><span><label>Badge Gold:</label></span><span>"+
                    "<input type='text' class='form-control' value='"+response_stack.items[0].badge_counts.gold+"' disabled></div>" +
                    "<div class='col-lg-12'><span><label>User Id:</label></span><span>"+
                    "<input type='text' class='form-control' value='"+response_stack.items[0].user_id+"' disabled></div>" +
                    "</div>");
                } else {
                    $("#modal").modal("hide");
                    $("#modal_stack").modal("show");
                    $("#modal-body-stackoverflow").attr("height",200);
                    $("#modal_stack .modal-body ").html("<div class='text-danger alert-danger h1 text-center'>Not found</div>");
                }
            },
            error : function (response) {
                $("#modal").modal("hide");
                $("#modal_stack").modal("show");
                $("#modal-body-stackoverflow").attr("height",500);
                $("#modal_stack .modal-body ").html("<div class='text-danger alert-danger h1 text-center'>Invalid Id</div>" +
                "<div>Error Message:"+response.responseJSON.error_message+"</div>");
            }
        }).fail(function () {
            $("#modal").modal("hide");
            $("#modal_stack").modal("show");
            $("#modal-body-stackoverflow").attr("height",500);
            $("#modal_stack .modal-body ").html("<div class='text-danger alert-danger h1 text-center'>Not found</div>");
        });
        $("#close_stack").click(function() {
            $("#loader").addClass("hidden");
            $("#modal .modal-body, #modal .modal-header, #modal .modal-footer").removeClass("hidden");
            $("#modal_stack").modal("hide");
            $("#modal").modal("show");
        });
    });
});

$(document).ready(function() {
    $("#logout").click(function () {
        $.ajax({
            url: "logout",
            type: "GET",
        });
    });
});