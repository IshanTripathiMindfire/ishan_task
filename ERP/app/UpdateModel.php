<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class UpdateModel extends Model
{
    /**
     * Get data from DB and pass on to controller
     * @param $id
     * @return mixed
     */
    public function getdata_model($id) {
        return DB::table(DB::raw('employee AS e'))->select(
        'e.PK_id','prefix','first_name','middle_name','last_name','email','date_of_birth','password','stackoverflow_id',
        'a1.street as r_street','cn1.name as r_city','s1.name as r_state','a1.zip_code as r_zip',
        'a2.street as o_street','cn2.name as o_city','s2.name as o_state','a2.zip_code as o_zip',
        'c1.numbers as telephone',
        'c2.numbers as mobile',
        'et.name as designation',
        'ec.name as company')
        ->leftJoin (DB::raw('address AS a1'), 'e.PK_id', '=', 'a1.FK_employee_id', 'AND', 'a1.type', '=', '"Residential"')
        ->leftJoin (DB::raw('address AS a2'), 'e.PK_id', '=', 'a2.FK_employee_id', 'AND', 'a2.type', '=', '"Official"')
        ->leftJoin (DB::raw('contact AS c1'), 'e.PK_id', '=', 'c1.FK_employee_id', 'AND', 'c1.type', '=', '"Telephone"')
        ->leftJoin (DB::raw('contact AS c2'), 'e.PK_id', '=', 'c2.FK_employee_id', 'AND', 'c2.type', '=', '"Mobile"')
        ->leftJoin (DB::raw('employment_type AS et'), 'et.PK_id', '=', 'e.FK_employment_type_id')
        ->leftJoin (DB::raw('employment_company AS ec'), 'ec.PK_id', '=', 'e.FK_employment_company_id')
        ->leftJoin (DB::raw('city AS cn1'),  'cn1.PK_id', '=', 'a1.FK_city_id', 'AND', 'a1.type', '=', '"Residential"')
        ->leftJoin (DB::raw('city AS cn2'), 'cn2.PK_id', '=', 'a2.FK_city_id', 'AND',  'a2.type', '=', '"Official"')
        ->leftJoin (DB::raw('state AS s1'), 's1.PK_id', '=', 'cn1.FK_state_id', 'AND',  'a1.type', '=', '"Residential"')
        ->leftJoin (DB::raw('state AS s2'), 's2.PK_id', '=', 'cn2.FK_state_id', 'AND', 'a2.type', '=', '"Official"')
        ->where('e.PK_id','=',$id)->get();
    }
}

