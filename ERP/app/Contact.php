<?php

namespace App;

use Illuminate\Contracts\Logging\Log;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contact';
    protected $primaryKey = 'PK_ID';
    //public $timestamps = false;
    const CREATED_AT = 'date_created';
    const UPDATED_AT = 'date_updated';

    /**
     * Store Contact to DB
     * @param $number
     * @param $type
     * @param $id
     * @return null
     */
    public function store($number,$type,$id) {
        try {
            $this -> numbers = $number;
            $this -> type = $type;
            $this -> FK_employee_id = $id;
            $this -> save();
        } catch(\Exception $e) {
            Log::error($e);
        }
            return null;
    }

    /**
     * Update DB with Contact details
     * @param $number
     * @param $type
     * @param $id
     */
    public function updateDB($number,$type,$id) {
        Contact::where('FK_employee_id', $id)
            ->where('type', $type)
            ->update(['numbers' => $number]);
    }
    /**
     * Relationship Employee-contact
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function employee() {
        return $this -> hasOne('App\Employee', 'FK_employee_id', 'PK_id');
    }

}
