<?php

namespace App;

use Illuminate\Contracts\Logging\Log;
use Illuminate\Database\Eloquent\Model;
use DB;

class EmployeeCommunication extends Model
{
    protected $table = 'employee_communication';
    protected $primaryKey = 'PK_ID';
    public $timestamps = false;
    const CREATED_AT = 'date_created';
    const UPDATED_AT = 'date_updated';

    /**
     * Store Communication medium to DB
     * @param $comm_medium
     * @param $id
     * @return null
     */
    public function store($comm_medium,$id) {
        try {
            //Inserting communication medium into DB
            foreach ($comm_medium as $cm) {
                if ($cm == 'SMS') {
                    $comm_id = 1;
                    DB::table('employee_communication') -> insert(['FK_employee_id' => $id,'FK_communication_type_id' => $comm_id]);
                } else if ($cm == 'Call') {
                    $comm_id = 2;
                    DB::table('employee_communication') -> insert(['FK_employee_id' => $id,'FK_communication_type_id' => $comm_id]);
                } else if ($cm == 'Email') {
                    $comm_id = 3;
                    DB::table('employee_communication') -> insert(['FK_employee_id' => $id,'FK_communication_type_id' => $comm_id]);
                }
            }
            return null;
        } catch (\Exception $e) {
            Log::error($e);
        }
        return null;
    }
}