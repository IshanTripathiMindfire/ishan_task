<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Contracts\Logging\Log;

class DeleteModel extends Model
{
    /**
     * Delete data from DB
     * @param $id
     */
    public function delete_data($id){
       try{
           DB::beginTransaction();
           DB::table('employee')->where('PK_id', '=', $id)->delete();
           DB::table('contact')->where('FK_employee_id', '=', $id)->delete();
           DB::table('employee_communication')->where('FK_employee_id', '=', $id)->delete();
           DB::table('address')->where('FK_employee_id', '=', $id)->delete();
           DB::commit();
       }catch(\Exception $e) {
           DB::rollBack();
           Log::info('DB deletion fail' . $e);
       }
   }
}
