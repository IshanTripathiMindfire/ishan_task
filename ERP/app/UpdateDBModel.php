<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class UpdateDBModel extends Model
{
    /**
     * Transaction Model for Update functionality
     * @param $request
     * @return string
     */
    public function updateDB($request) {

        try {
            DB::beginTransaction();

            $r_state = new State();
            $r_state_id = $r_state -> store($request -> update_r_state);
            $o_state = new State();
            $o_state_id = $o_state -> store($request -> update_o_state);

            $r_city = new City();
            $r_city_id = $r_city -> store($request -> update_r_city, $r_state_id);
            $o_city = new City();
            $o_city_id = $o_city -> store($request -> update_o_city, $o_state_id);

            $e_type = new EmploymentType();
            $type_id = $e_type -> store($request -> update_designation);

            $company = new EmploymentCompany();
            $company_id = $company -> store($request -> update_company);

            $employee = new Employee();
            $employee_id = $employee -> updateDB($request,$company_id,$type_id);

            $t_contact = new Contact();
            $contact_id = $t_contact -> updateDB($request -> update_telephone,'Telephone',$request -> update_id);
            $m_contact = new Contact();
            $company_id = $m_contact -> updateDB($request -> update_mobile,'Mobile',$request -> update_id);

            $r_address = new Address();
            $address_id = $r_address -> updateDB($request -> update_r_zip_code, $request -> update_r_street, $r_city_id, 'Residential', $request -> update_id);
            $o_address = new Address();
            $address_id = $o_address -> updateDB($request -> update_o_zip_code, $request -> update_o_street, $o_city_id, 'Official', $request -> update_id);

            DB::commit();

            return "true";

        } catch(\Exception $e) {
            DB::rollBack();
            Log::info('DB insertion fail' . $e);
            return "false";
        }
    }
}
