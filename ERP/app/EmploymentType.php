<?php

namespace App;

use Illuminate\Contracts\Logging\Log;
use Illuminate\Database\Eloquent\Model;

class EmploymentType extends Model
{
    protected $table = 'employment_type';
    protected $primaryKey = 'PK_ID';
    //public $timestamps = false;
    const CREATED_AT = 'date_created';
    const UPDATED_AT = 'date_updated';

    /**
     * Store Designation to DB
     * @param $designation
     * @return mixed
     */
    public function store($designation) {

        try {
            if ( ! (EmploymentType::where('name', '=', $designation) -> exists())){
                $this -> name = $designation;
                $this -> save();
                $id = EmploymentType::where('name', '=', $designation) -> first();
                return $id['PK_id'];
            }
            else {
                $id = EmploymentType::where('name', '=', $designation) -> first();
                return $id['PK_id'];
            }
        } catch (\Exception $e) {
            Log::error($e);
        }

    }

    /**
     * Relationship Employee and Designation
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function employee() {
        return $this -> hasMany('App\Employee', 'FK_employment_type_id', 'Pk_id');
    }
}

