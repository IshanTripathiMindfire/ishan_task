<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class City extends Model
{
    protected $table = 'city';
    protected $primaryKey = 'PK_ID';
    //public $timestamps = false;
    const CREATED_AT = 'date_created';
    const UPDATED_AT = 'date_updated';


    /**
     * Store city to DB
     * @param $city_name
     * @param $state_id
     * @return mixed
     */
    public function store($city_name,$state_id) {

        try {
            if ( ! (City::where('name', '=', $city_name, 'AND', 'FK_state_id', '=', $state_id)->exists())) {
                $this -> name = $city_name;
                $this -> FK_state_id = $state_id;
                $this -> save();
                $id = City::where('name', '=', $city_name, 'AND', 'FK_state_id', '=', $state_id)->first();
                return $id['PK_id'];
            } else {
                $id = City::where('name', '=', $city_name, 'AND', 'FK_state_id', '=', $state_id)->first();
                return $id['PK_id'];
            }
        } catch (\Exception $e) {
            Log::error($e);
        }
    }

    /**
     * Address relationship with city table
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function address() {
        return $this -> hasOne('App\Address', 'PK_id', 'FK_city_id');
    }

    /**
     * State relationship with city
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function state() {
        return $this -> belongsTo('App\State', 'FK_state_id', 'Pk_id');
    }
}
