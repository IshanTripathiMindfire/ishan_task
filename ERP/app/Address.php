<?php

namespace App;

use Illuminate\Contracts\Logging\Log;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'address';
    protected $primaryKey = 'PK_ID';
    //public $timestamps = false;
    const CREATED_AT = 'date_created';
    const UPDATED_AT = 'date_updated';


    /**
     * Store address to DB
     * @param $zip_code
     * @param $street
     * @param $c_id
     * @param $type
     * @param $e_id
     * @return null
     */
    public function store($zip_code,$street,$c_id,$type,$e_id) {

        try {
            $this->zip_code = $zip_code;
            $this->street = $street;
            $this->FK_city_id = $c_id;
            $this->type = $type;
            $this->FK_employee_id = $e_id;
            $this->save();
        } catch (\Exception $e) {
            Log::error($e);
        }
        return null;
    }

    /**
     * update address in DB
     * @param $zip_code
     * @param $street
     * @param $c_id
     * @param $type
     * @param $e_id
     */
    public function updateDB($zip_code,$street,$c_id,$type,$e_id) {
        Address::where('FK_employee_id', $e_id)
            ->where('type', $type)
            ->update(['zip_code' => $zip_code, 'street' => $street, 'FK_city_id' => $c_id]);
    }

    /**
     * Relationship function with employee
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function employee() {
        return $this -> hasOne('App\Employee', 'FK_employee_id', 'PK_id');
    }

    /**
     * Relationship function with city
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function city() {
        return $this -> hasMany('App\City', 'PK_id', 'FK_city_id');
    }

}
