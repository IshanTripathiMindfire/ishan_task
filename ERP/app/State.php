<?php

namespace App;

use App\Http\Requests\RegistrationRequest;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Database\Eloquent\Model;
use DB;

class State extends Model
{
    protected $table = 'state';
    protected $primaryKey = 'PK_ID';
    //public $timestamps = false;
    const CREATED_AT = 'date_created';
    const UPDATED_AT = 'date_updated';

    /**
     * Store State to DB
     * @param $request
     * @return mixed
     */
    public function store($request) {

        try {
            if ( ! (State::where('name', '=', $request)->exists())) {
                $this -> name = $request;
                $this -> save();
                $id = State::where('name', '=', $request)->first();
                return $id['PK_id'];
            } else {
                $id = State::where('name', '=', $request)->first();
                return $id['PK_id'];
            }
        } catch (\Exception $e) {
            Log::error($e);
        }
    }

    /**
     * Relatioship State - City
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function city() {
        return $this -> hasMany('App\City', 'FK_state_id', 'Pk_id');
    }
}
