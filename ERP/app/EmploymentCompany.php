<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class EmploymentCompany extends Model
{
    protected $table = 'employment_company';
    protected $primaryKey = 'PK_ID';
    //public $timestamps = false;
    const CREATED_AT = 'date_created';
    const UPDATED_AT = 'date_updated';

    /**
     * Store Company to DB
     * @param $company
     * @return mixed
     */
    public function store($company) {

        try {
            if ( ! (EmploymentCompany::where('name', '=', $company)->exists())) {
                $this -> name = $company;
                $this -> save();
                $id = EmploymentCompany::where('name', '=', $company)->first();
                return $id['PK_id'];
            }
            else{
                $id = EmploymentCompany::where('name', '=', $company)->first();
                return $id['PK_id'];
            }
        }  catch (\Exception $e) {
            Log::error($e);
        }
    }

    /**
     * Relationship employee and company
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function employee() {
        return $this -> hasMany('App\Employee', 'FK_employment_company_id', 'Pk_id');
    }
}
