<?php

namespace App;

use Log;
use Illuminate\Database\Eloquent\Model;
use App\Address;
use App\Contact;
use App\Employee;
use App\EmploymeeCommunication;
use App\EmploymentCompany;
use App\State;
use App\City;
use App\EmploymentType;
use DB;

class Transactions extends Model
{
    /**
     * Insert store values
     * @param $request
     * @return bool
     */
    public function store($request) {

        try {
            DB::beginTransaction();

            $r_state = new State();
            $r_state_id = $r_state -> store($request -> residential_state);
            $o_state = new State();
            $o_state_id = $o_state -> store($request -> official_state);

            $r_city = new City();
            $r_city_id = $r_city -> store($request -> residential_city, $r_state_id);
            $o_city = new City();
            $o_city_id = $o_city -> store($request -> official_city, $o_state_id);

            $e_type = new EmploymentType();
            $type_id = $e_type -> store($request -> designation);

            $company = new EmploymentCompany();
            $company_id = $company -> store($request -> company);

            $employee = new Employee();
            $employee_id = $employee -> store($request,$company_id,$type_id);

            $t_contact = new Contact();
            $contact_id = $t_contact -> store($request -> telephone,'Telephone',$employee_id);
            $m_contact = new Contact();
            $company_id = $m_contact -> store($request -> mobile,'Mobile',$employee_id);

            $r_address = new Address();
            $address_id = $r_address -> store($request -> residential_zip_code, $request -> residential_address, $r_city_id, 'Residential', $employee_id);
            $o_address = new Address();
            $address_id = $o_address -> store($request -> official_zip_code, $request -> official_address, $o_city_id, 'Official', $employee_id);

            $comm = new EmployeeCommunication();
            $comm_id = $comm -> store($request -> communication_medium, $employee_id);

            DB::commit();

            return true;

        } catch(\Exception $e) {
            DB::rollBack();
            Log::info('DB insertion fail' . $e);
            return false;
        }
    }
}
