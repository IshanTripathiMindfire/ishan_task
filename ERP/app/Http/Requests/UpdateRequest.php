<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'update_dob' => 'required|date|before:2005-01-01',
            'update_r_street' => 'required|alpha_num',
            'update_r_city' => 'required|alpha',
            'update_r_state' => 'required|alpha',
            'update_r_zip_code' => 'required|between:6,10',
            'update_o_street' => 'required|alpha_num',
            'update_o_city' => 'required|alpha',
            'update_o_state' => 'required|alpha',
            'update_o_zip_code' => 'required|between:6,10',
            'update_telephone' => 'required|digits:10',
            'update_mobile' => 'required|digits:10',
            'update_designation' => 'required|alpha',
            'update_company' => 'required|alpha',
            'update_stackoverflow_id' => 'between:0,15'
        ];
    }
}
