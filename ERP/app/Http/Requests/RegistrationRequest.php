<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RegistrationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'prefix' => 'required',
            'first_name' => 'required|alpha',
            'middle_name' => 'alpha',
            'last_name' => 'required|alpha',
            'gender' => 'required',
            'marital_status' => 'required',
            'date_of_birth' => 'date|before:2005-01-01',
            'stack_overflow_id' => 'between:0,15',
            'telephone' => 'required|digits:10',
            'mobile' => 'required|digits:10',
            'email' => 'required|email',
            'fax' => 'digits:10',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
            'residential_address' => 'required|alpha_num',
            'residential_city' => 'required|alpha',
            'residential_state' => 'required|alpha',
            'residential_zip_code' => 'required|between:6,10',
            'designation' => 'required|alpha',
            'company' => 'required|alpha',
            'official_address' => 'required|alpha_num',
            'official_city' => 'required|alpha',
            'official_state' => 'required|alpha',
            'official_zip_code' => 'required|between:6,10',
            'communication_medium' => 'required'
        ];
    }
}
