<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 21/3/17
 * Time: 12:49 PM
 */

namespace App\Http\Controllers;


use App\DashboardModel;
use App\Http\Requests\UpdateRequest;

use App\User;
use App;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Auth;
use Hash;
use Session;

class SiteController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Auth user
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function views () {

        if(Auth::check()) {
            return view('/layouts/dashboard') -> with ('data',$this->getdata_db());
        } else {
            return view('/layouts/login');
        }

    }

    /**
     * Login to dashboard page
     * @param Request $request
     * @return $this
     */
    public function dashboard (Request $request) {
        $this->validate($request, [
            'login_email_input' => 'required | email',
            'login_password_input' => 'required',
        ]);
        $credentials = array( 'email' => $request -> login_email_input, 'password' => $request -> login_password_input);

        if (Auth::attempt($credentials)) {

            return view('/layouts/dashboard') -> with ('data',$this->getdata_db());
        }
        else {
            return view('/layouts/login') -> withErrors("User not register.");
        }


    }


    /**
     * Session login redirect to dashboard
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getDashboard() {
        if (Auth::check()) {
            return view('/layouts/dashboard') -> with ('data',$this->getdata_db());
        } else {
            return redirect('/');
        }
    }

    /**
     * Logout and destroy session
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logout() {
        Session::flush();
        Auth::logout();
        return redirect('/');
    }

    /**
     * Get data from DB and return to view dashboard
     * @return mixed
     */
    public function getdata_db(){
        $data = new App\Employee;
        return $data -> getdata();
    }

    /**
     * Get data from DB and return to view Modal box
     * @param $id_request
     * @return mixed
     */
    public function data_to_model($id_request){
        $data = new App\DataModel();
        return $data -> getdata_model($id_request);

    }

    /**
     * Get data from DB and return to Update modal box
     * @param $id_request
     * @return mixed
     */
    public function update_model($id_request){
        $data = new App\UpdateModel();
        return $data -> getdata_model($id_request);

    }

    public function update_db_data(UpdateRequest $request){

        $update = new App\UpdateDBModel();
        $commit = $update -> updateDB($request);
        return $commit;

    }

    /**
     * Delete Employee from DB
     * @param $id_request
     * @return App\DeleteModel
     */
    public function delete_model($id_request){
        $data = new App\DeleteModel();
        $data -> delete_data($id_request);
        return $data;

    }

}