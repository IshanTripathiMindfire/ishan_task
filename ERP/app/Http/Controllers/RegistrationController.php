<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 21/3/17
 * Time: 12:38 PM
 */

namespace App\Http\Controllers;


use App\Http\Requests\RegistrationRequest;
use App\State;
use App\Transactions;
use App\User;
use Hash;
use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use Auth;
use DB;


class RegistrationController extends Controller
{
    /**
     * Function to display registration page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function views() {
        return view('/layouts/registration');
    }

    /**
     * Function to store and validate registration data
     * @param RegistrationRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(RegistrationRequest $request) {

        $transaction = new Transactions();
        $commit = $transaction -> store($request);

        if($commit) {
            return view('layouts/login');
        } else {
            return view('layouts/error');
        }

    }
}