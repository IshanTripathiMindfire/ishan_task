<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'SiteController@views');

Route::get('logout', 'SiteController@logout');

Route::get('registration', 'RegistrationController@views');

Route::post('dashboard', 'SiteController@dashboard');

Route::get('dashboard', 'SiteController@getDashboard');

Route::post('register', 'RegistrationController@store');

Route::get('modal_data/{id_request}',['uses' =>'SiteController@data_to_model']);

Route::get('modal_data_update/{id_request}',['uses' =>'SiteController@update_model']);

Route::post('update_db', ['uses' => 'SiteController@update_db_data']);

Route::get('delete_data/{id_request}',['uses' =>'SiteController@delete_model']);