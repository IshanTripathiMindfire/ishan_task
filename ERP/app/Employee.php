<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Hash;
use DB;
use Log;

class Employee extends Model
{
    protected $table = 'employee';
    protected $primaryKey = 'PK_ID';
    //public $timestamps = false;
    const CREATED_AT = 'date_created';
    const UPDATED_AT = 'date_updated';


    /**
     * Store Employee to DB
     * @param $request
     * @param $company_id
     * @param $type_id
     * @return mixed
     */
    public function store($request, $company_id, $type_id) {

        try {
            $this -> prefix = $request -> prefix;
            $this -> first_name = $request -> first_name;
            $this -> middle_name = $request -> middle_name;
            $this -> last_name = $request -> last_name;
            $this -> gender = $request -> gender;
            $this -> password = Hash::make($request->password);
            $this -> stackoverflow_id = $request -> stack_overflow_id;
            $this -> FK_employment_company_id = $company_id;
            $this -> FK_employment_type_id = $type_id;
            $this -> marital_status = $request -> marital_status;
            $this -> fax = $request -> fax;
            $this -> email = $request -> email;
            $this -> date_of_birth = $request -> date_of_birth;
            $this->save();
            $id = Employee::where('email', '=', $request -> email) -> first();
            return $id['PK_id'];
        } catch (\Exception $e) {
            Log::error($e);
        }

    }


    /**
     * Update DB with Employee Details
     * @param $request
     * @param $company_id
     * @param $type_id
     */
    public function updateDB($request, $company_id, $type_id) {
        Employee::where('PK_id', $request -> update_id)
            ->update(['stackoverflow_id' => $request -> update_stackoverflow_id, 'FK_employment_company_id' => $company_id, 'FK_employment_type_id' => $type_id, 'date_of_birth' => $request -> update_dob]);
    }

    /**
     * Fetch Data from DB
     * @return mixed
     */
    public function getdata(){
        return DB::table(DB::raw('employee AS e'))->select(
            'e.PK_id',
            DB::raw('concat(prefix," ",first_name," ",middle_name," ",last_name) as name'),
            'email',
            'stackoverflow_id',
            DB::raw('concat(a1.street," ",cn1.name," ",s1.name," ",a1.zip_code) as residential_address'),
            DB::raw('concat(a2.street," ",cn2.name," ",s2.name," ",a2.zip_code) as official_address'),
            'c1.numbers as telephone',
            'c2.numbers as mobile',
            'et.name as designation',
            'ec.name as company'
        )
            ->leftJoin (DB::raw('address AS a1'), 'e.PK_id', '=', 'a1.FK_employee_id', 'AND', 'a1.type', '=', '"Residential"')
            ->leftJoin (DB::raw('address AS a2'), 'e.PK_id', '=', 'a2.FK_employee_id', 'AND', 'a2.type', '=', '"Official"')
            ->leftJoin (DB::raw('contact AS c1'), 'e.PK_id', '=', 'c1.FK_employee_id', 'AND', 'c1.type', '=', '"Telephone"')
            ->leftJoin (DB::raw('contact AS c2'), 'e.PK_id', '=', 'c2.FK_employee_id', 'AND', 'c2.type', '=', '"Mobile"')
            ->leftJoin (DB::raw('employment_type AS et'), 'et.PK_id', '=', 'e.FK_employment_type_id')
            ->leftJoin (DB::raw('employment_company AS ec'), 'ec.PK_id', '=', 'e.FK_employment_company_id')
            ->leftJoin (DB::raw('city AS cn1'),  'cn1.PK_id', '=', 'a1.FK_city_id', 'AND', 'a1.type', '=', '"Residential"')
            ->leftJoin (DB::raw('city AS cn2'), 'cn2.PK_id', '=', 'a2.FK_city_id', 'AND',  'a2.type', '=', '"Official"')
            ->leftJoin (DB::raw('state AS s1'), 's1.PK_id', '=', 'cn1.FK_state_id', 'AND',  'a1.type', '=', '"Residential"')
            ->leftJoin (DB::raw('state AS s2'), 's2.PK_id', '=', 'cn2.FK_state_id', 'AND', 'a2.type', '=', '"Official"')
            ->groupBy('e.PK_id')->get();
    }



    /**
     * Relationship Employee - Address
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function address() {
        return $this -> hasMany('App\Address', 'FK_employee_id', 'PK_id');
    }

    /**
     * Relationship Employee contact
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contact() {
        return $this -> hasMany('App\Contact', 'FK_employee_id', 'PK_id');
    }

    /**'
     * Relationship employee cand designation type
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employmenttype() {
        return $this -> belongsTo('App\EmploymentType', 'FK_employment_type_id', 'Pk_id');
    }

    /**
     * Relationship Employee and company
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employmentcompany() {
        return $this -> belongsTo('App\EmploymentCompany', 'FK_employment_company_id', 'Pk_id');
    }


}


