<?php
// This template part is to add and perform required functions
// Function to add bootstrap CDN
function Mytheme_enqueue_script() {
    wp_enqueue_style ( 'bootstrap-css', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
        array(), '1.0', 'all' );
    wp_enqueue_script ( 'jquery', 'https://code.jquery.com/jquery-3.1.1.min.js', array(), '1.0', true );
    wp_enqueue_script ( 'bootstrap-js', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js',
        array('jquery'), '1.0', true );
    wp_enqueue_style ( "style",get_template_directory_uri().'/css/style.css',array(),'1.0','all' );
    wp_enqueue_script ( "script",get_template_directory_uri().'/js/script.js',array(),'1.0','all' );
}

add_action ( 'wp_enqueue_scripts', 'Mytheme_enqueue_script' );

// Function to setup header menu
function mytheme_setup() {
    register_nav_menu ( 'header-menu','Menu for header' );
}

add_action ( 'init','mytheme_setup' );

// Function to set up sidebar
function mytheme_sidebar() {
    register_sidebar ( array(
        'name' => 'my-widget',
        'id'   => 'mywidget',
        'description'   => 'This is a widgetized area.',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>'
    ));
}

add_action ( 'widgets_init','mytheme_sidebar' );


// To get number of views
function getpost_views($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}

// To set number of views
function post_views($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
