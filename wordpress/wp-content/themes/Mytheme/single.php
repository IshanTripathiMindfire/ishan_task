<?php

// Single.php file display all the details and views for the blog
get_header();

?>
<div class="row">
    <div class="col-lg-6 col-md-6">
        <?php
            if ( have_posts() ) {
                while ( have_posts() ) {
                    the_post();
        ?>
        <h2> <?php the_title() ?> </h2><br>
        <p> <?php the_content() ?> </p><br>
        <?php post_views(get_the_ID()); ?>
        <?php echo "<h1>".getpost_views(get_the_ID())."</h1>"; ?>
        <p> Posted on: <?php the_time('F,j,Y') ?> in <?php the_category() ?> </p><br>
    </div>
    <div class="col-lg-6 col-md-6 visible-lg visible-md">
        <?php
            } // end while
            } // end if
            get_sidebar();
        ?>
    </div>
</div>
<?php
    echo "<div class='col-lg-12'>";
    get_footer();
?>