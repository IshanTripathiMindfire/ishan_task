<?php

// This template is to create header and open html tag for pages

?>

<!DOCTYPE html5>
<html>
	<head>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<title>This is my theme</title>
        <?php wp_head(); ?>
	</head>
	
	<body>
        <div class="container">
            <div id="header" class="row">
            <?php
                echo "<div class='col-lg-12 h1  text-center'><u>".get_bloginfo('name')."</u></div>";
                echo "<div class='col-lg-12 h3  text-center'>".get_bloginfo()."</div><br>";
                echo "</div>";
                echo "<div class='row'>";
                wp_nav_menu();
                echo "</div>";
            ?>
