﻿<?php
/*
 *Forgot password page layout
 */
?>
<!DOCTYPE html>
<html>

    <head>
        <meta name = "viewport" content = "width=device-width, initial-scale=1">
        <?php include 'include.php'; ?>
		<title>Forgot Password Page</title>
	</head>

    <body>
        <div class="container alert alert-info" id="forgot_container">
            <form action="forgot_password_email.php" method="post" >
                <div class="row">
                    <div class="col-lg-12">
                        <p class="h1 text-center"><u>FORGOT PASSWORD</u></p>
                    </div>
                </div>
                <div class="row" id = "forgot_box">
                    <div class="col-lg-2">
                        <label class="form-inline">Enter the Email Id:</label>
                    </div>

                    <div class="col-lg-6">
                        <input type="email" name="email_forgot"  class="form-control" required></input>
                    </div>
                </div>
                <div class="row" id="forgot_button">
                    <div class="col-lg-1 col-md-2 col-sm-6 col-xs-6" >
                       <input type="submit" class="btn-primary btn-lg" ></input>
                    </div>
                    <div class=" col-lg-offset-1 col-lg-1 col-md-2 col-sm-6 col-xs-6">
                        <button type="button" name="back" id="back_button" onclick="window.history.back();" class="btn-primary btn-lg">Back
                        </button>
                    </div>
                </div>
            </form>
        </div>
	</body>
</html>
