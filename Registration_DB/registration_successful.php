<?php
/**
 * After successful registration
 */
session_start();
session_unset();
?>
<html>
    <head>
        <title>Registration successful</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php
            // Including all the required CDN
            include "include.php";
        ?>

    </head>

    <body>
        <div class="container">
            <div class="row" id="success_alert">
                <div class="col-lg-12 alert alert-success">
                    <p class =" h1 text-center">Success! You're Done </p><br>
                    <p class="h3 text-center">You've successfully registered your profile app account.</p>
                </div>
            </div>
            <div class="row">

                <div class="col-lg-5 col-md-12 alert alert-info">
                    <div class="h4 text-center" id="verify_messgae">
                        <p id="link_message">We request you to verify the registered email id.<br>By clicking on the link mail to your registered email.</p>
                        <p><a href="./index.php">Click</a> here to redirect to login page. </p>
                    </div>
                </div>
                <div class="col-lg-offset-1 col-lg-6 col-md-12 alert alert-info visible-lg">
                    <div class="img-responsive text-center"><img src = "./success.gif" height="50%"></div>
                </div>
            </div>
        </div>
    </body>
</html>
