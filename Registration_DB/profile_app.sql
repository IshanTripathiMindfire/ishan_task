-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 17, 2017 at 11:17 AM
-- Server version: 5.5.54-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `profile_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE IF NOT EXISTS `address` (
  `PK_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `zip_code` varchar(7) COLLATE utf8_unicode_ci NOT NULL,
  `street` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `FK_city_id` int(10) unsigned NOT NULL,
  `type` enum('Official','Residential') COLLATE utf8_unicode_ci NOT NULL,
  `FK_employee_id` int(10) unsigned NOT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`PK_id`),
  KEY `city_id` (`FK_city_id`),
  KEY `FK_employee_id` (`FK_employee_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=203 ;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`PK_id`, `zip_code`, `street`, `FK_city_id`, `type`, `FK_employee_id`, `date_created`, `date_updated`) VALUES
(4, '8787', 'HHJH', 14, 'Residential', 40, '2017-01-31 09:28:08', '2017-01-31 09:28:08'),
(6, '8787', 'HHJH', 14, 'Residential', 41, '2017-01-31 09:29:14', '2017-01-31 09:29:14'),
(11, '898', 'KJK', 15, 'Official', 41, '2017-01-31 09:56:36', '2017-01-31 09:56:36'),
(12, '8787', 'HHJH', 14, 'Residential', 42, '2017-01-31 09:57:54', '2017-01-31 09:57:54'),
(13, '898', 'KJK', 15, 'Official', 42, '2017-01-31 09:57:54', '2017-01-31 09:57:54'),
(14, '8787', 'HHJH', 14, 'Residential', 43, '2017-01-31 10:04:55', '2017-01-31 10:04:55'),
(15, '898', 'KJK', 15, 'Official', 43, '2017-01-31 10:04:55', '2017-01-31 10:04:55'),
(16, '8787', 'HHJH', 14, 'Residential', 44, '2017-01-31 10:06:24', '2017-01-31 10:06:24'),
(17, '898', 'KJK', 15, 'Official', 44, '2017-01-31 10:06:24', '2017-01-31 10:06:24'),
(26, '22', 'HHJH', 14, 'Residential', 45, '2017-01-31 10:12:04', '2017-01-31 10:12:04'),
(27, '22', 'KJK', 15, 'Official', 45, '2017-01-31 10:12:04', '2017-01-31 10:12:04'),
(28, '22', 'HHJH', 14, 'Residential', 46, '2017-01-31 10:12:57', '2017-01-31 10:12:57'),
(29, '22', 'KJK', 15, 'Official', 46, '2017-01-31 10:12:57', '2017-01-31 10:12:57'),
(30, '22', 'HHJH', 14, 'Residential', 47, '2017-01-31 10:13:46', '2017-01-31 10:13:46'),
(31, '22', 'KJK', 15, 'Official', 47, '2017-01-31 10:13:46', '2017-01-31 10:13:46'),
(88, '22', 'BHJB', 30, 'Residential', 72, '2017-02-01 14:36:08', '2017-02-01 14:36:08'),
(91, '22', 'BHJB', 30, 'Residential', 75, '2017-02-01 14:36:20', '2017-02-01 14:36:20'),
(175, '4443', 'ASD', 71, 'Residential', 118, '2017-02-07 12:43:29', '2017-02-16 09:42:41'),
(176, '7510241', 'ORISSA', 9, 'Official', 118, '2017-02-07 12:43:29', '2017-02-16 09:42:41'),
(197, '22', '', 96, 'Residential', 129, '2017-02-16 13:50:43', '2017-02-16 13:50:43'),
(198, '33', '', 97, 'Official', 129, '2017-02-16 13:50:43', '2017-02-16 13:50:43'),
(201, '834001', 'SOMEWHERE SAFE', 68, 'Residential', 131, '2017-02-16 14:12:33', '2017-02-16 14:12:33'),
(202, '751010', 'SOMEWHERE ABOVE THE EARTH and below sky', 100, 'Official', 131, '2017-02-16 14:12:33', '2017-02-16 14:14:35');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `PK_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `FK_state_id` int(10) unsigned NOT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`PK_id`),
  KEY `state_id` (`FK_state_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=101 ;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`PK_id`, `name`, `FK_state_id`, `date_created`, `date_updated`) VALUES
(1, 'Kanpur', 1, '2017-01-30 12:31:37', '2017-01-30 12:31:37'),
(2, 'Lucknow', 1, '2017-01-30 12:31:37', '2017-01-30 12:31:37'),
(8, '', 9, '2017-01-30 15:38:24', '2017-01-30 15:38:24'),
(9, 'BHUBANESWAR', 2, '2017-01-31 08:50:17', '2017-01-31 08:50:17'),
(10, 'ETWA', 1, '2017-01-31 08:55:15', '2017-01-31 08:55:15'),
(11, 'ROU', 10, '2017-01-31 08:56:38', '2017-01-31 08:56:38'),
(12, 'ETWAQW', 11, '2017-01-31 09:19:03', '2017-01-31 09:19:03'),
(13, 'ROUQW', 12, '2017-01-31 09:19:04', '2017-01-31 09:19:04'),
(14, 'ETWAQWQQQ', 13, '2017-01-31 09:24:42', '2017-01-31 09:24:42'),
(15, 'QQQYYYY', 14, '2017-01-31 09:24:43', '2017-01-31 09:24:43'),
(16, 'EEEE', 15, '2017-01-31 13:33:02', '2017-01-31 13:33:02'),
(17, 'HB BVGFG', 16, '2017-01-31 13:33:02', '2017-01-31 13:33:02'),
(18, 'EEEEEEEEEEEEEEEEEEEE', 17, '2017-01-31 14:01:33', '2017-01-31 14:01:33'),
(19, 'HB BVGFGEEEE', 18, '2017-01-31 14:01:33', '2017-01-31 14:01:33'),
(20, 'JHJ', 19, '2017-01-31 17:11:57', '2017-01-31 17:11:57'),
(21, 'JJJJ', 20, '2017-01-31 17:11:57', '2017-01-31 17:11:57'),
(22, 'JHJMNJHJHJ', 21, '2017-01-31 18:12:10', '2017-01-31 18:12:10'),
(23, 'JJJJ', 22, '2017-01-31 18:12:10', '2017-01-31 18:12:10'),
(24, 'B', 23, '2017-02-01 06:03:35', '2017-02-01 06:03:35'),
(25, 'BHB', 23, '2017-02-01 06:03:35', '2017-02-01 06:03:35'),
(27, 'SSASSSB', 25, '2017-02-01 13:25:09', '2017-02-01 13:25:09'),
(29, 'SSASSSBQQQWW', 25, '2017-02-01 13:25:39', '2017-02-01 13:25:39'),
(30, 'SSASSSBQQQWWSS', 25, '2017-02-01 13:26:17', '2017-02-01 13:26:17'),
(31, 'BHBQQQDDSS', 25, '2017-02-01 13:30:38', '2017-02-01 13:30:38'),
(32, 'BHBQQQDDSS', 25, '2017-02-01 13:31:03', '2017-02-01 13:31:03'),
(33, 'BHBQQQDDSS', 25, '2017-02-01 13:31:22', '2017-02-01 13:31:22'),
(34, 'BHBQQQDDSS', 25, '2017-02-01 13:34:16', '2017-02-01 13:34:16'),
(35, 'BHBQQQDDSS', 25, '2017-02-01 13:34:19', '2017-02-01 13:34:19'),
(36, 'BHBQQQDDSS', 25, '2017-02-01 13:34:22', '2017-02-01 13:34:22'),
(37, 'BHBQQQDDSS', 25, '2017-02-01 13:36:23', '2017-02-01 13:36:23'),
(38, 'BHBQQQDDSS', 25, '2017-02-01 13:36:53', '2017-02-01 13:36:53'),
(39, 'BHBQQQDDSS', 25, '2017-02-01 13:37:00', '2017-02-01 13:37:00'),
(40, 'BHBQQQDDSS', 25, '2017-02-01 13:37:20', '2017-02-01 13:37:20'),
(41, 'BHBQQQDDSS', 25, '2017-02-01 13:38:55', '2017-02-01 13:38:55'),
(42, 'BHBQQQDDSS', 25, '2017-02-01 13:40:02', '2017-02-01 13:40:02'),
(43, 'BHBQQQDDSS', 25, '2017-02-01 13:40:15', '2017-02-01 13:40:15'),
(44, 'BHBQQQDDSS', 25, '2017-02-01 13:40:38', '2017-02-01 13:40:38'),
(45, 'BHBQQQDDSS', 25, '2017-02-01 13:40:46', '2017-02-01 13:40:46'),
(46, 'BHBQQQDDSS', 25, '2017-02-01 13:41:00', '2017-02-01 13:41:00'),
(47, 'BHBQQQDDSS', 25, '2017-02-01 13:47:55', '2017-02-01 13:47:55'),
(48, 'BHBQQQDDSS', 25, '2017-02-01 13:49:22', '2017-02-01 13:49:22'),
(49, 'BHBQQQDDSS', 25, '2017-02-01 13:51:35', '2017-02-01 13:51:35'),
(50, 'BHBQQQDDSS', 25, '2017-02-01 13:52:41', '2017-02-01 13:52:41'),
(51, 'BHBQQQDDSS', 26, '2017-02-01 13:57:27', '2017-02-01 13:57:27'),
(52, 'SD', 27, '2017-02-01 15:25:24', '2017-02-01 15:25:24'),
(53, 'FDG', 28, '2017-02-01 15:28:57', '2017-02-01 15:28:57'),
(54, '98', 29, '2017-02-02 14:10:45', '2017-02-02 14:10:45'),
(55, '9', 30, '2017-02-02 14:10:45', '2017-02-02 14:10:45'),
(60, 'H', 35, '2017-02-02 16:40:24', '2017-02-02 16:40:24'),
(61, 'JH', 35, '2017-02-02 16:40:24', '2017-02-02 16:40:24'),
(62, 'MM', 36, '2017-02-02 16:52:21', '2017-02-02 16:52:21'),
(63, 'HGJ', 37, '2017-02-03 06:03:36', '2017-02-03 06:03:36'),
(64, 'HG', 38, '2017-02-03 06:03:36', '2017-02-03 06:03:36'),
(67, 'TRIVENDRUM', 6, '2017-02-03 07:19:56', '2017-02-03 07:19:56'),
(68, 'RANCHI', 39, '2017-02-07 12:43:29', '2017-02-07 12:43:29'),
(69, 'ETWAQWSS', 11, '2017-02-16 09:39:46', '2017-02-16 09:39:46'),
(70, 'BHUBANESWARSS', 2, '2017-02-16 09:39:46', '2017-02-16 09:39:46'),
(71, 'ETWAQW', 1, '2017-02-16 09:42:41', '2017-02-16 09:42:41'),
(94, 'WE', 62, '2017-02-16 13:37:34', '2017-02-16 13:37:34'),
(95, 'EW', 63, '2017-02-16 13:37:34', '2017-02-16 13:37:34'),
(96, 'DS', 64, '2017-02-16 13:50:43', '2017-02-16 13:50:43'),
(97, 'DSD', 65, '2017-02-16 13:50:43', '2017-02-16 13:50:43'),
(98, 'SDS', 65, '2017-02-16 13:53:01', '2017-02-16 13:53:01'),
(99, 'SA', 66, '2017-02-16 13:53:01', '2017-02-16 13:53:01'),
(100, 'BHUBANESWAR', 67, '2017-02-16 14:12:33', '2017-02-16 14:12:33');

-- --------------------------------------------------------

--
-- Table structure for table `communication_type`
--

CREATE TABLE IF NOT EXISTS `communication_type` (
  `PK_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`PK_id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `communication_type`
--

INSERT INTO `communication_type` (`PK_id`, `type`) VALUES
(2, 'Call'),
(3, 'Email'),
(1, 'SMS');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `PK_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('Telephone','Mobile') COLLATE utf8_unicode_ci NOT NULL,
  `FK_employee_id` int(10) unsigned NOT NULL,
  `numbers` varchar(13) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`PK_id`),
  KEY `FK_employee_id` (`FK_employee_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=155 ;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`PK_id`, `type`, `FK_employee_id`, `numbers`, `date_created`, `date_updated`) VALUES
(1, 'Telephone', 45, '12311', '2017-01-31 10:12:41', '2017-01-31 10:12:41'),
(2, 'Telephone', 46, '12311', '2017-01-31 10:12:57', '2017-01-31 10:12:57'),
(3, 'Telephone', 47, '12311', '2017-01-31 10:13:46', '2017-01-31 10:13:46'),
(145, 'Telephone', 118, '9829389238', '2017-02-07 12:43:29', '2017-02-16 09:42:41'),
(146, 'Mobile', 118, '189183292', '2017-02-07 12:43:29', '2017-02-16 09:39:46'),
(149, 'Telephone', 129, '222', '2017-02-16 13:50:43', '2017-02-16 13:50:43'),
(150, 'Mobile', 129, '222', '2017-02-16 13:50:43', '2017-02-16 13:50:43'),
(153, 'Telephone', 131, '9692020128', '2017-02-16 14:12:33', '2017-02-16 14:12:33'),
(154, 'Mobile', 131, '9692020128', '2017-02-16 14:12:33', '2017-02-16 14:12:33');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `PK_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`PK_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `PK_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `prefix` enum('Mr.','Mrs.','Ms') COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `middle_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `gender` enum('Male','Female') COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `FK_employment_type_id` int(10) unsigned NOT NULL,
  `FK_employment_company_id` int(10) unsigned NOT NULL,
  `picture_location` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `marital_status` enum('Single','Married','Divorced') COLLATE utf8_unicode_ci NOT NULL,
  `extra_notes` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fax` int(10) unsigned NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_birth` date NOT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`PK_id`),
  KEY `employment_type_id` (`FK_employment_type_id`),
  KEY `employment_company_id` (`FK_employment_company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=132 ;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`PK_id`, `prefix`, `first_name`, `middle_name`, `last_name`, `gender`, `password`, `FK_employment_type_id`, `FK_employment_company_id`, `picture_location`, `marital_status`, `extra_notes`, `fax`, `email`, `date_of_birth`, `date_created`, `date_updated`) VALUES
(40, 'Mr.', 'ISHAN', '', 'KJK', 'Male', '123', 13, 10, '', 'Single', '', 7878, '', '2017-01-04', '2017-01-31 09:28:08', '2017-01-31 09:28:08'),
(41, 'Mr.', 'ISHAN', '', 'KJK', 'Male', '123', 13, 10, '', 'Single', '', 7878, '', '2017-01-04', '2017-01-31 09:29:14', '2017-01-31 09:29:14'),
(42, 'Mr.', 'ISHAN', '', 'KJK', 'Male', '123', 13, 10, '', 'Single', '', 7878, '', '2017-01-04', '2017-01-31 09:57:54', '2017-01-31 09:57:54'),
(43, 'Mr.', 'ISHAN', '', 'KJK', 'Male', '123', 13, 10, '', 'Single', '', 7878, '', '2017-01-04', '2017-01-31 10:04:55', '2017-01-31 10:04:55'),
(44, 'Mr.', 'ISHAN', '', 'KJK', 'Male', '123', 13, 10, '', 'Single', '', 7878, '', '2017-01-04', '2017-01-31 10:06:24', '2017-01-31 10:06:24'),
(45, 'Mr.', 'ISHAN1', '', 'KJK1', 'Male', '111', 13, 10, '', 'Single', '', 11111, '', '2017-01-04', '2017-01-31 10:12:04', '2017-01-31 10:12:04'),
(46, 'Mr.', 'ISHAN1', '', 'KJK1', 'Male', '111', 13, 10, '', 'Single', '', 11111, '', '2017-01-04', '2017-01-31 10:12:57', '2017-01-31 10:12:57'),
(47, 'Mr.', 'ISHAN1', '', 'KJK1', 'Male', '111', 13, 10, '', 'Single', '', 11111, '', '2017-01-04', '2017-01-31 10:13:46', '2017-01-31 10:13:46'),
(67, 'Mr.', 'IJHJ', 'HJKH', 'JHJ', 'Male', '1', 19, 15, '', 'Single', '', 22, '', '2017-02-02', '2017-02-01 14:26:43', '2017-02-01 14:26:43'),
(68, 'Mr.', 'IJHJ', 'HJKH', 'JHJ', 'Male', '1', 19, 15, '', 'Single', '', 22, '', '2017-02-02', '2017-02-01 14:26:48', '2017-02-01 14:26:48'),
(69, 'Mr.', 'IJHJ', 'HJKH', 'JHJ', 'Male', '1', 19, 15, '', 'Single', '', 22, '', '2017-02-02', '2017-02-01 14:26:51', '2017-02-01 14:26:51'),
(70, 'Mr.', 'IJHJ', 'HJKH', 'JHJ', 'Male', '1', 19, 15, '', 'Single', '', 22, '', '2017-02-02', '2017-02-01 14:34:31', '2017-02-01 14:34:31'),
(71, 'Mr.', 'IJHJ', 'HJKH', 'JHJ', 'Male', '1', 19, 15, '', 'Single', '', 22, '', '2017-02-02', '2017-02-01 14:35:17', '2017-02-01 14:35:17'),
(72, 'Mr.', 'IJHJ', 'HJKH', 'JHJ', 'Male', '1', 19, 15, '', 'Single', '', 22, '', '2017-02-02', '2017-02-01 14:36:08', '2017-02-01 14:36:08'),
(75, 'Mr.', 'IJHJ', 'HJKH', 'JHJ', 'Male', '1', 19, 15, '', 'Single', '', 22, '', '2017-02-02', '2017-02-01 14:36:20', '2017-02-01 14:36:20'),
(118, 'Mr.', 'ATUL', '', 'KUMAR', 'Male', 'mindfire', 28, 76, '', 'Single', '', 0, 'atulk@mindfiresolutions.com', '2016-07-05', '2017-02-07 12:43:29', '2017-02-16 11:47:53'),
(129, 'Mr.', 'SSD', 'SD', 'DS', 'Male', '11', 11, 90, '', 'Single', '', 2, 'ersr@efgh.com', '2017-02-10', '2017-02-16 13:50:43', '2017-02-16 13:50:43'),
(131, 'Mr.', 'SINGH', 'A', 'K', 'Male', 'mindfire', 42, 94, '', 'Single', '', 0, 'atul.kr.singh1989@gmail.com', '1989-12-31', '2017-02-16 14:12:33', '2017-02-16 14:27:54');

-- --------------------------------------------------------

--
-- Table structure for table `employee_communication`
--

CREATE TABLE IF NOT EXISTS `employee_communication` (
  `FK_employee_id` int(10) unsigned NOT NULL,
  `FK_communication_type_id` int(10) unsigned NOT NULL,
  KEY `employee_id` (`FK_employee_id`),
  KEY `FK_communication_type_id` (`FK_communication_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employee_communication`
--

INSERT INTO `employee_communication` (`FK_employee_id`, `FK_communication_type_id`) VALUES
(118, 3),
(129, 1),
(131, 2),
(131, 3);

-- --------------------------------------------------------

--
-- Table structure for table `employment_company`
--

CREATE TABLE IF NOT EXISTS `employment_company` (
  `PK_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`PK_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=95 ;

--
-- Dumping data for table `employment_company`
--

INSERT INTO `employment_company` (`PK_id`, `name`, `date_created`, `date_updated`) VALUES
(1, 'jh', NULL, '2017-01-30 09:04:50'),
(2, 'jh', NULL, '2017-01-30 09:08:01'),
(3, 'jh', NULL, '2017-01-30 09:09:22'),
(7, 'SD', '2017-01-30 14:26:27', '2017-01-30 14:26:27'),
(8, '', '2017-01-30 15:36:04', '2017-01-30 15:36:04'),
(9, 'KM', '2017-01-31 08:44:07', '2017-01-31 08:44:07'),
(10, 'SAD', '2017-01-31 08:46:24', '2017-01-31 08:46:24'),
(11, 'GH', '2017-01-31 13:33:02', '2017-01-31 13:33:02'),
(12, 'GHEE', '2017-01-31 14:01:33', '2017-01-31 14:01:33'),
(13, 'JHJ', '2017-01-31 17:11:57', '2017-01-31 17:11:57'),
(14, 'BH', '2017-02-01 06:03:35', '2017-02-01 06:03:35'),
(15, 'BHQQQ', '2017-02-01 14:15:54', '2017-02-01 14:15:54'),
(16, '8', '2017-02-02 14:10:45', '2017-02-02 14:10:45'),
(19, 'H', '2017-02-02 16:40:24', '2017-02-02 16:40:24'),
(20, 'HG', '2017-02-03 06:03:36', '2017-02-03 06:03:36'),
(23, 'TEST COMPANY', '2017-02-03 07:19:56', '2017-02-03 07:19:56'),
(24, 'M F S', '2017-02-07 12:43:29', '2017-02-07 12:43:29'),
(39, 'M F S', '2017-02-16 08:29:04', '2017-02-16 08:29:04'),
(40, 'M F S', '2017-02-16 08:48:10', '2017-02-16 08:48:10'),
(41, 'M F S', '2017-02-16 08:48:16', '2017-02-16 08:48:16'),
(42, 'M F S', '2017-02-16 08:48:41', '2017-02-16 08:48:41'),
(43, 'M F S', '2017-02-16 08:49:04', '2017-02-16 08:49:04'),
(44, 'M F S', '2017-02-16 09:12:54', '2017-02-16 09:12:54'),
(45, 'M F S', '2017-02-16 09:12:55', '2017-02-16 09:12:55'),
(46, 'M F S', '2017-02-16 09:13:47', '2017-02-16 09:13:47'),
(47, 'M F S', '2017-02-16 09:14:08', '2017-02-16 09:14:08'),
(48, 'M F S', '2017-02-16 09:14:31', '2017-02-16 09:14:31'),
(49, 'M F S', '2017-02-16 09:15:01', '2017-02-16 09:15:01'),
(50, 'M F S', '2017-02-16 09:16:35', '2017-02-16 09:16:35'),
(51, 'M F S', '2017-02-16 09:17:22', '2017-02-16 09:17:22'),
(52, 'M F S', '2017-02-16 09:17:24', '2017-02-16 09:17:24'),
(53, 'M F S', '2017-02-16 09:17:32', '2017-02-16 09:17:32'),
(54, 'M F S', '2017-02-16 09:17:34', '2017-02-16 09:17:34'),
(55, 'M F S', '2017-02-16 09:17:34', '2017-02-16 09:17:34'),
(56, 'M F S', '2017-02-16 09:17:34', '2017-02-16 09:17:34'),
(57, 'M F S', '2017-02-16 09:17:34', '2017-02-16 09:17:34'),
(58, 'M F S', '2017-02-16 09:17:35', '2017-02-16 09:17:35'),
(59, 'M F S', '2017-02-16 09:17:37', '2017-02-16 09:17:37'),
(60, 'M F S', '2017-02-16 09:18:50', '2017-02-16 09:18:50'),
(61, 'M F S', '2017-02-16 09:18:50', '2017-02-16 09:18:50'),
(62, 'M F S', '2017-02-16 09:18:50', '2017-02-16 09:18:50'),
(63, 'M F S', '2017-02-16 09:18:51', '2017-02-16 09:18:51'),
(64, 'M F S', '2017-02-16 09:18:51', '2017-02-16 09:18:51'),
(65, 'TEST COMPANY', '2017-02-16 09:19:22', '2017-02-16 09:19:22'),
(66, 'M F S', '2017-02-16 09:20:25', '2017-02-16 09:20:25'),
(67, 'M F S', '2017-02-16 09:20:53', '2017-02-16 09:20:53'),
(68, 'M F S', '2017-02-16 09:25:28', '2017-02-16 09:25:28'),
(69, 'M F S', '2017-02-16 09:27:32', '2017-02-16 09:27:32'),
(70, 'M F S', '2017-02-16 09:28:00', '2017-02-16 09:28:00'),
(71, 'M F S', '2017-02-16 09:29:41', '2017-02-16 09:29:41'),
(72, 'M F S', '2017-02-16 09:39:46', '2017-02-16 09:39:46'),
(73, 'M F S', '2017-02-16 09:42:41', '2017-02-16 09:42:41'),
(74, 'M F S', '2017-02-16 11:31:49', '2017-02-16 11:31:49'),
(75, 'M F S', '2017-02-16 11:32:03', '2017-02-16 11:32:03'),
(76, 'M F S', '2017-02-16 11:47:53', '2017-02-16 11:47:53'),
(77, 'HG', '2017-02-16 11:50:16', '2017-02-16 11:50:16'),
(89, 'EW', '2017-02-16 13:37:34', '2017-02-16 13:37:34'),
(90, 'ASDS', '2017-02-16 13:50:43', '2017-02-16 13:50:43'),
(91, 'SDF', '2017-02-16 13:53:01', '2017-02-16 13:53:01'),
(92, 'MINDFIRE SOLUTIONS', '2017-02-16 14:12:33', '2017-02-16 14:12:33'),
(93, 'MINDFIRE SOLUTIONSs', '2017-02-16 14:14:35', '2017-02-16 14:14:35'),
(94, 'MINDFIRE SOLUTIONSs', '2017-02-16 14:27:54', '2017-02-16 14:27:54');

-- --------------------------------------------------------

--
-- Table structure for table `employment_type`
--

CREATE TABLE IF NOT EXISTS `employment_type` (
  `PK_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`PK_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=43 ;

--
-- Dumping data for table `employment_type`
--

INSERT INTO `employment_type` (`PK_id`, `name`, `date_created`, `date_updated`) VALUES
(1, 'h', NULL, '2017-01-30 09:02:32'),
(3, 'h', NULL, '2017-01-30 09:06:23'),
(4, 'h', NULL, '2017-01-30 09:08:01'),
(5, 'h', NULL, '2017-01-30 09:09:22'),
(9, '', '2017-01-30 14:11:27', '2017-01-30 14:11:27'),
(10, 'SOFTWARE DEVELOPER', '2017-01-30 14:22:54', '2017-01-30 14:22:54'),
(11, 'SD', '2017-01-30 15:40:53', '2017-01-30 15:40:53'),
(12, '989', '2017-01-31 08:44:07', '2017-01-31 08:44:07'),
(13, 'SSAD', '2017-01-31 08:46:24', '2017-01-31 08:46:24'),
(14, 'BHBH', '2017-01-31 13:33:02', '2017-01-31 13:33:02'),
(15, 'BHBHEEE', '2017-01-31 14:01:33', '2017-01-31 14:01:33'),
(16, 'NJH', '2017-01-31 17:11:57', '2017-01-31 17:11:57'),
(17, 'BHB', '2017-02-01 06:03:35', '2017-02-01 06:03:35'),
(18, 'BHBWWWWWWWWWWWW', '2017-02-01 14:06:00', '2017-02-01 14:06:00'),
(19, 'BHBWWWWWWWWWWWWQQ', '2017-02-01 14:15:27', '2017-02-01 14:15:27'),
(20, '89', '2017-02-02 14:10:45', '2017-02-02 14:10:45'),
(23, 'JHJJ', '2017-02-02 16:40:24', '2017-02-02 16:40:24'),
(24, 'G', '2017-02-03 06:03:36', '2017-02-03 06:03:36'),
(27, 'TEST ENGINEER', '2017-02-03 07:19:56', '2017-02-03 07:19:56'),
(28, 'S S E', '2017-02-07 12:43:29', '2017-02-07 12:43:29'),
(40, 'EW', '2017-02-16 13:37:34', '2017-02-16 13:37:34'),
(41, 'DSFDG', '2017-02-16 13:53:01', '2017-02-16 13:53:01'),
(42, 'SENIOR SOFTWARE ENGINEER', '2017-02-16 14:12:33', '2017-02-16 14:12:33');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE IF NOT EXISTS `state` (
  `PK_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`PK_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=68 ;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`PK_id`, `name`, `date_created`, `date_updated`) VALUES
(1, 'Uttar Pradesh', '2017-01-30 12:30:10', '2017-01-30 12:30:10'),
(2, 'Odisha', '2017-01-30 12:30:10', '2017-01-30 12:30:10'),
(5, 'New Delhi', '2017-01-30 13:10:00', '2017-01-30 13:10:00'),
(6, 'Kerala', '2017-01-30 13:13:19', '2017-01-30 13:13:19'),
(7, 'BIHAR', '2017-01-30 13:22:48', '2017-01-30 13:22:48'),
(9, '', '2017-01-30 15:36:03', '2017-01-30 15:36:03'),
(10, 'AP', '2017-01-31 08:55:15', '2017-01-31 08:55:15'),
(11, 'UTTAR PRADESHQW', '2017-01-31 09:19:03', '2017-01-31 09:19:03'),
(12, 'APQW', '2017-01-31 09:19:03', '2017-01-31 09:19:03'),
(13, 'QQQQQQ', '2017-01-31 09:24:42', '2017-01-31 09:24:42'),
(14, 'QQQQQQQQ', '2017-01-31 09:24:42', '2017-01-31 09:24:42'),
(15, 'EEEERRRR', '2017-01-31 13:33:02', '2017-01-31 13:33:02'),
(16, 'GHGH', '2017-01-31 13:33:02', '2017-01-31 13:33:02'),
(17, 'EEEERRRREEEEEEE', '2017-01-31 14:01:33', '2017-01-31 14:01:33'),
(18, 'GHGHEEEEE', '2017-01-31 14:01:33', '2017-01-31 14:01:33'),
(19, 'JHJJJJ', '2017-01-31 17:11:56', '2017-01-31 17:11:56'),
(20, 'BBBB', '2017-01-31 17:11:57', '2017-01-31 17:11:57'),
(21, 'JJHJHJHNN', '2017-01-31 18:12:10', '2017-01-31 18:12:10'),
(22, 'BBBBNJNJN', '2017-01-31 18:12:10', '2017-01-31 18:12:10'),
(23, 'HB', '2017-02-01 06:03:34', '2017-02-01 06:03:34'),
(24, 'QWERTYUIOEEEEQQQQQQQQQ', '2017-02-01 11:51:41', '2017-02-01 11:51:41'),
(25, 'QWERTYUIOEEEEQQQQQQQQQWWW', '2017-02-01 12:25:48', '2017-02-01 12:25:48'),
(26, 'HBQAZXSW', '2017-02-01 13:30:38', '2017-02-01 13:30:38'),
(27, 'SA', '2017-02-01 15:25:24', '2017-02-01 15:25:24'),
(28, 'DSFG', '2017-02-01 15:28:57', '2017-02-01 15:28:57'),
(29, '98', '2017-02-02 14:10:45', '2017-02-02 14:10:45'),
(30, '89', '2017-02-02 14:10:45', '2017-02-02 14:10:45'),
(35, 'JH', '2017-02-02 16:40:24', '2017-02-02 16:40:24'),
(36, 'MMM', '2017-02-02 16:52:21', '2017-02-02 16:52:21'),
(37, 'GH', '2017-02-03 06:03:36', '2017-02-03 06:03:36'),
(38, 'HG', '2017-02-03 06:03:36', '2017-02-03 06:03:36'),
(39, 'JHARKHAND', '2017-02-07 12:43:29', '2017-02-07 12:43:29'),
(62, 'WEW', '2017-02-16 13:37:34', '2017-02-16 13:37:34'),
(63, 'EW', '2017-02-16 13:37:34', '2017-02-16 13:37:34'),
(64, 'DSD', '2017-02-16 13:50:43', '2017-02-16 13:50:43'),
(65, 'SAD', '2017-02-16 13:50:43', '2017-02-16 13:50:43'),
(66, 'DAS', '2017-02-16 13:53:01', '2017-02-16 13:53:01'),
(67, 'ORISSA', '2017-02-16 14:12:33', '2017-02-16 14:12:33');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `address`
--
ALTER TABLE `address`
  ADD CONSTRAINT `address_ibfk_1` FOREIGN KEY (`FK_employee_id`) REFERENCES `employee` (`PK_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `address_ibfk_2` FOREIGN KEY (`FK_city_id`) REFERENCES `city` (`PK_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `CK_address_city` FOREIGN KEY (`FK_city_id`) REFERENCES `city` (`PK_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `CK_address_employee` FOREIGN KEY (`FK_employee_id`) REFERENCES `employee` (`PK_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `city`
--
ALTER TABLE `city`
  ADD CONSTRAINT `city_ibfk_1` FOREIGN KEY (`FK_state_id`) REFERENCES `state` (`PK_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `CK_city_state` FOREIGN KEY (`FK_state_id`) REFERENCES `state` (`PK_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `contact`
--
ALTER TABLE `contact`
  ADD CONSTRAINT `contact_ibfk_1` FOREIGN KEY (`FK_employee_id`) REFERENCES `employee` (`PK_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `CK_contact_employee` FOREIGN KEY (`FK_employee_id`) REFERENCES `employee` (`PK_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `employee`
--
ALTER TABLE `employee`
  ADD CONSTRAINT `CK_employee_employee_company` FOREIGN KEY (`FK_employment_company_id`) REFERENCES `employment_company` (`PK_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `CK_employee_employee_type` FOREIGN KEY (`FK_employment_type_id`) REFERENCES `employment_type` (`PK_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `employee_communication`
--
ALTER TABLE `employee_communication`
  ADD CONSTRAINT `employee_communication_ibfk_1` FOREIGN KEY (`FK_employee_id`) REFERENCES `employee` (`PK_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `employee_communication_ibfk_2` FOREIGN KEY (`FK_communication_type_id`) REFERENCES `communication_type` (`PK_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `CK_employee_communication_communication_type` FOREIGN KEY (`FK_communication_type_id`) REFERENCES `communication_type` (`PK_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `CK_employee_communication_employee` FOREIGN KEY (`FK_employee_id`) REFERENCES `employee` (`PK_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
