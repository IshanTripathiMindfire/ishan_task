<?php
session_start();
require_once 'db_conn.php';
try {
    $stmt = $conn->prepare("SELECT * FROM employee WHERE email=:email");
    $stmt->bindParam(':email', $_POST["email_id_login"]);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    if ( $stmt -> rowCount() > 0 && $result['password'] == md5($_POST['password_login']) && $result['verify_status'] == 1) {
        $_SESSION['name'] = $result['prefix'] . ' ' . $result['first_name'] . ' ' . $result['middle_name'] . ' ' . $result['last_name'];
        $_SESSION['email_id'] = $result['email'];
        $_SESSION['error'] = 'already_logged_in';
        header("Location:http://" . $_SERVER["SERVER_NAME"] . "/php/Registration_DB/dashboard.php");
    }
    else {
        if ($result['verify_status'] == 0)
            $_SESSION['error'] = 'account not verifiyed';
        else if ($result['verify_status'] == 2) {
            $_SESSION['error'] = "password reset";
        }
        else
            $_SESSION['error'] = 'invalid_data';

        header("Location:http://" . $_SERVER["SERVER_NAME"] . "/php/Registration_DB/index.php");
    }
} catch (exception $e) {
    echo $e->getMessage();
}
