<?php
/*
 * Get data from DB and push it on to modal update
 * */
    $id = $_POST["id"];
    require_once 'db_conn.php';
    $query = 'Select e.PK_id,prefix,first_name,middle_name,last_name,email,date_of_birth,password,stackoverflow_id,
                a1.street as r_street,cn1.name as r_city,s1.name as r_state,a1.zip_code as r_zip,
                a2.street as o_street,cn2.name as o_city,s2.name as o_state,a2.zip_code as o_zip,
                c1.numbers as telephone,
                c2.numbers as mobile,
                et.name as designation,
                ec.name as company
                from employee e
                left join address a1 on e.PK_id=a1.FK_employee_id AND a1.type="Residential"
                left join address a2 on e.PK_id=a2.FK_employee_id AND a2.type="Official"
                left join contact c1 on e.PK_id=c1.FK_employee_id AND c1.type="Telephone"
                left join contact c2 on e.PK_id=c2.FK_employee_id AND c2.type="Mobile"
                left join employment_type et on et.PK_id=e.FK_employment_type_id
                left join employment_company ec on ec.PK_id=e.FK_employment_company_id
                left join city cn1 on cn1.PK_id=a1.FK_city_id AND a1.type="Residential"
                left join city cn2 on cn2.PK_id=a2.FK_city_id AND a2.type="Official"
                left join state s1 on s1.PK_id=cn1.FK_state_id AND a1.type="Residential"
                left join state s2 on s2.PK_id=cn2.FK_state_id AND a2.type="Official"
                where e.PK_id=' . $id;
    $stmt = $conn->prepare($query);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    echo json_encode($result);
