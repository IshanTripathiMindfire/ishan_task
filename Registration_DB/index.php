<?php
/*
 *login page layout
 */
?>

<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php include 'include.php';?>
		<title>Login Page</title>
	</head>

    <body>

        <div class="container">

            <nav role="navigation" class="navbar navbar-default">

                <div class="navbar-header">

                    <button type="button" data-target="#navbarCollapse" data-toggle="collapse" aria-expanded="false" aria-controls="navbarCollapse" class="navbar-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a href="#" class="navbar-brand">Login</a>
                </div>

                <div id="navbarCollapse" class="collapse navbar-collapse ">
                    <ul class="nav navbar-nav">
                        <li><a href="./registration_page.php">Register</a></li>
                    </ul>

                </div>
            </nav>

            <form action="verify_login.php" method="post" id="login">
                <div class="col-lg-offset-3 col-lg-6"  >

                    <?php
session_start();
// Redirecting to dashboard page if session is logged.
if ($_SESSION["error"] == "already_logged_in") {
	header("Location:http://" . $_SERVER["SERVER_NAME"] . "/php/Registration_DB/dashboard.php");
}

// Displaying error if password is wrong
else if ($_SESSION["error"] == "invalid_data") {
	echo '<h4 class="alert alert-danger text-danger">*********Invalid Login Details*********</h4>';
	session_unset();
} else if ($_SESSION["error"] == "account not verifiyed") {
	echo '<h4 class="alert alert-danger text-danger">*********Account Not Verifiyed*********</h4>';
	session_unset();
} else if ($_SESSION["error"] == "reset_link_send") {
	echo '<h4 class="alert alert-success">*********Password Reset Link Successfully Send*********</h4>';
	session_unset();
} else if ($_SESSION["error"] == "invalid email") {
	echo '<h4 class="alert alert-danger text-danger">*********Invalid Email*********</h4>';
	session_unset();
} else if ($_SESSION["error"] == "password reset") {
	echo '<h4 class="alert alert-warning ">Password reset request was generated.Please use link mailed to reset password  or re-generate the link using forgot password</h4>';
	session_unset();
} else if ($_SESSION["error"] == "Password Reset Successful") {
	echo '<h4 class="alert alert-success ">*********Password Reset Successful*********</h4>';
	session_unset();
}
?>
                    <fieldset class="scheduler-border">
                        <legend><center><h1> Login</h1></center></legend>

                        <div class="row" >

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                <span><label>Email Id:</label></span>
                                <span id="email_id_login"><input type="text" name="email_id_login" id="emailid" class="form-control" required></input></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                <span><label>Password:</label></span>
                                <span id="password_login"><input type="password" name="password_login" class="form-control" required></input></span>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <input type="submit" name="submit" id="login_button" class="btn-primary btn-lg pull-left" value="Login"/>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <b class="text-right pull-right"><a href="./forgot_password_page.php">
                                Forgot Password?</a></b>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </form>
            <div class=" col-lg-offset-5 col-sm-offset-4 col-xs-offset-2 col-lg-5 col-md-12 col-sm-12 col-xs-12">
                <button id="facebookbtn" class="loginBtn loginBtn--facebook">
                    Login with Facebook
                </button>
            </div>
        </div>
	</body>
</html>
