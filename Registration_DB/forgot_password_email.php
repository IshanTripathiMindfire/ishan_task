<?php
require_once 'db_conn.php';
session_start();
try {
    $stmt = $conn -> prepare("SELECT * FROM employee WHERE email=:email");
    $stmt->bindParam(':email',$_POST['email_forgot']);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    if ($stmt->rowCount() != 0) {

        $stmt = $conn->prepare("UPDATE employee SET verify_status = 2 WHERE email=:email");
        $stmt->bindParam(':email', $result['email']);
        $stmt->execute();
        $hash = md5($result['first_name'] . $result['email'] . $result['PK_id'] . $result['date_updated']);
        $verify_link = "http://" . $_SERVER['SERVER_NAME'] . "/php/Registration_DB/reset_password_page.php?email=" . $result['email'] . "&&hash=" . $hash;

        $messgae = "Hi " . $result['prefix'] . " " . $result['last_name'] . " ,<br>
                    <b>Password Reset link :</b><a href=" . $verify_link . ">" . $verify_link."</a>
                    <br><br>Regards,<br>Profile App team";

        mail($result['email'],"Password Reset",$messgae,"From: Admin <mindfireishan@gmail.com>"."\r\n"."Content-type: text/html; charset=>utf-8");
        $_SESSION['error'] = "reset_link_send";
        $stmt = $conn->prepare("UPDATE employee SET hash = :hash WHERE email=:email");
        $stmt->bindParam(':email', $_POST['email_forgot']);
        $stmt->bindParam(':hash', $hash);
        $stmt->execute();

    }
    else
    {
        $_SESSION['error'] = "invalid email";
    }
    header("Location:http://" . $_SERVER["SERVER_NAME"] . "/php/Registration_DB/index.php");
} catch (exception $e) {
    echo $e->getMessage();
}
