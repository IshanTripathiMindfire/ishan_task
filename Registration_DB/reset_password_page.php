<?php
/*
 * Reset password page layout and link verification
 */

require_once 'db_conn.php';
session_start();
try {
    $stmt = $conn->prepare("SELECT * FROM employee WHERE email=:email");
    $stmt->bindParam(':email',$_GET['email']);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    if ($stmt->rowCount() == 0 || $_GET['hash'] != $result['hash'] || $result['verify_status'] != 2 ) {
        $messgae = "Hi " . $result['prefix'] . " " . $result['last_name'] . ",<br>
                    <p>Someone is trying to change your profile app password.<br> If it's not you who have generated this request.Kindly login and change the password.</p><br>
                    Regards,<br>Profile App Team";
        mail($result['email'],"Reuse of password reset link",$messgae,"From: Admin <mindfireishan@gmail.com>"."\r\n"."Content-type: text/html; charset=>utf-8");
        header("Location:http://" . $_SERVER["SERVER_NAME"] . "/php/Registration_DB/index.php");
    }
    else {
        $_SESSION['forgot_pass_email'] = $_GET['email'];
    }
} catch (exception $e) {
    echo $e->getMessage();
}
?>
<!DOCTYPE html>
<html>

    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php include 'include.php'; ?>
		<title>Reset Password Page</title>
	</head>

    <body>

        <div class="container">

            <form action="reset_password.php" method="post">

                <div class="col-lg-6" id="login_box" >

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 well" id="header">
                            <h1 class="text-center">Reset Password</h1>
                        </div>
                    </div>

                    <div class="row well">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 well">
                            <span><label>New Password:</label></span>
                            <span id="reset_password"><input type="password" name="reset_password"
                            class="form-control" required></input></span>
                        </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 well" >
                        <span><label>Confirm Password:</label></span>
                        <span id="reset_confirm_password"><input type="password" name="reset_confirm_password"
                        class="form-control" required></input></span>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <center><input type="submit" name="submit" id="reset_button"
                                class="btn-primary btn-lg" value="Reset"/>
                        </center>
                    </div>
                </div>
            </form>
        </div>
	</body>
</html>
