<?php
	/*
	 * Reset the user password into DB
	 * */
	session_start();

	if ($_POST["reset_password"] == $_POST["reset_confirm_password"]) {
		try {
			require_once 'db_conn.php';
			$conn->beginTransaction();
			$stmt = $conn->prepare("UPDATE employee SET password=:password_value WHERE email=:email");
			$stmt->bindParam(':password_value', md5($_POST["reset_password"]));
			$stmt->bindParam(':email', $_SESSION["forgot_pass_email"]);
			$stmt->execute();
			$conn->commit();
            $stmt = $conn->prepare("UPDATE employee SET verify_status = 1 WHERE email=:email");
            $stmt->bindParam(':email', $_SESSION["forgot_pass_email"]);
            $stmt->execute();
			$_SESSION["error"] = "Password Reset Successful";
			header("Location:http://" . $_SERVER["SERVER_NAME"] . "/php/Registration_DB/index.php");
		} catch (exception $e) {
            logError($e->getMessage());
            echo "Password not reset Internal ERROR";
			$conn->rollback();
		}
	}

