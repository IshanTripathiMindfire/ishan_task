<?php
/*
     * Function to validate String using regular expression
     *
 */
function check_regular_expression($value, $error_string, $index) {
    global $errors;
    if ( ! preg_match("/^[A-Za-z ]*$/", $value)) {

        $errors[$index] = $error_string;

    }
}

/*
 * Function to validate Number using regular expression
 */
function check_regular_expression_number($value, $error_string, $index) {
    global $errors;
    if ( ! preg_match("/^[0-9 ]*$/", $value)) {

        $errors[$index] = $error_string;

    }
}