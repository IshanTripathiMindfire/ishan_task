<?php
/*
 * Delete data from DB
 */
try {
    require_once 'db_conn.php';
    $conn->beginTransaction();
    $stmt = $conn->prepare("DELETE FROM employee WHERE PK_id = :id");
    $stmt->bindParam(':id', $_POST["id"]);
    $stmt->execute();
    $stmt = $conn->prepare("DELETE FROM address WHERE FK_employee_id = :id");
    $stmt->bindParam(':id', $_POST["id"]);
    $stmt->execute();
    $stmt = $conn->prepare("DELETE FROM contact WHERE FK_employee_id = :id");
    $stmt->bindParam(':id', $_POST["id"]);
    $stmt->execute();
    $stmt = $conn->prepare("DELETE FROM employee_communication WHERE FK_employee_id = :id");
    $stmt->bindParam(':id', $_POST["id"]);
    $stmt->execute();
    $conn->commit();
} catch (exception $e) {
    logError($e->getMessage());
    echo "Data delete error";
    $conn->rollBack();

}
