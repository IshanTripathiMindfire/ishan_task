<?php
/*
 * DB connection file
 */
    include_once "errorlog.php";
    try {

        $conn = new PDO('mysql:host=localhost;dbname=profile_app;', 'ishan', 'mindfire');

        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    } catch (PdoException $ex) {
        logError($ex->getMessage());
        echo "DB Connection error";
    }