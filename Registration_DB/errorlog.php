<?php
/*
 * Error log file
 * Function to open and write log to it
 * */
function logError($error)
{
       $now = new DateTime();
       $dateTime = $now->format('[D M d H:i:s Y]');
       $text = "$dateTime \t $error \n";

       //create log file
       $logFile = fopen(realpath("error.log"), "a");

       //put error
       fwrite($logFile, $text);
       fclose($logFile);
}
