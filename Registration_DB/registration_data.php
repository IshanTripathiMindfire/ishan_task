<?php
    /*
     * File to validate data and insert into DB
     * */
    session_start();
    include 'functions.php';

    if ($_SESSION["error"] == "already_logged_in") {
        header("Location:http://" . $_SERVER["SERVER_NAME"] . "/php/Registration_DB/dashboard.php");
    }

    $errors = array();

    $_SESSION["prefix"] = $_POST["prefix_input"];
    $_SESSION["first_name"] = strtoupper($_POST["first_name"]);

    check_regular_expression($_SESSION["first_name"], "First name should contain alphabets only", 0);

    $_SESSION["middle_name"] = strtoupper($_POST["middle_name"]);

    check_regular_expression($_SESSION["middle_name"], "Middle name should contain alphabets only", 1);

    $_SESSION["last_name"] = strtoupper($_POST["last_name"]);

    check_regular_expression($_SESSION["last_name"], "Last name should contain alphabets only", 2);

    $_SESSION["gender"] = $_POST["gender"];
    $_SESSION["marital_status"] = $_POST["marital_status"];
    $_SESSION["date_of_birth"] = $_POST["date_of_birth"];

    if (strtotime(date("Y-m-d")) < strtotime($_SESSION["date_of_birth"])) {
        $errors[3] = "Date of birth should be less than today's date.";
    }

    $_SESSION["stack_overflow_id"] = $_POST["stack_overflow_id"];

    check_regular_expression_number($_SESSION["stack_overflow_id"], "Stack overflow id should contain only numbers.",4);

    $_SESSION["telephone"] = $_POST["telephone_input"];

    check_regular_expression_number($_SESSION["telephone"], "Telephone number should contain only numbers.", 5);

    $_SESSION["mobile"] = $_POST["mobile"];

    check_regular_expression_number($_SESSION["mobile"], "Mobile number should contain only numbers.", 6);

    $_SESSION["email"] = $_POST["email"];

    if (!filter_var($_SESSION["email"], FILTER_VALIDATE_EMAIL)) {
        $errors[7] = "Invalid email format";
    }

    $_SESSION["fax"] = $_POST["fax"];

    check_regular_expression_number($_SESSION["fax"], "Fax number should contain only numbers.", 8);

    $_SESSION["password"] = $_POST["password"];

    if (sizeof($_SESSION["password"]) > 8) {
        $errors[9] = "Password length should be greater than 8.";
    }

    $_SESSION["confirm_password"] = $_POST["confirm_password"];

    if ($_SESSION["password"] != $_SESSION["confirm_password"]) {
        $errors[10] = "password_mismatch";
    }

    $_SESSION["residential_address"] = strtoupper($_POST["residential_address_input"]);
    $_SESSION["residential_city"] = strtoupper($_POST["residential_city"]);

    check_regular_expression($_SESSION["residential_city"], "Residential city should contain alphabets only", 11);

    $_SESSION["residential_state"] = strtoupper($_POST["residential_state"]);

    check_regular_expression($_SESSION["residential_state"], "Residential state should contain alphabets only", 12);

    $_SESSION["residential_zip_code"] = $_POST["residential_zip_code"];

    check_regular_expression_number($_SESSION["residential_zip_code"], "Residential zip code should contain only numbers.", 13);

    $_SESSION["title"] = strtoupper($_POST["title"]);

    check_regular_expression($_SESSION["title"], "Title should contain alphabets only", 14);

    $_SESSION["company_name"] = strtoupper($_POST["company_name"]);

    check_regular_expression($_SESSION["company_name"], "Company name should contain alphabets only", 15);

    $_SESSION["office_address"] = strtoupper($_POST["office_address_input"]);
    $_SESSION["office_city"] = strtoupper($_POST["office_city"]);

    check_regular_expression($_SESSION["office_city"], "Office city should contain alphabets only", 16);

    $_SESSION["office_state"] = strtoupper($_POST["office_state"]);

    check_regular_expression($_SESSION["office_state"], "Office state should contain alphabets only", 17);

    $_SESSION["office_zip_code"] = $_POST["office_zip_code"];

    check_regular_expression_number($_SESSION["office_zip_code"], "Office zip code should contain only numbers.", 18);

    $_SESSION["communicate_medium1"] = $_POST["communicate_medium1"];
    $_SESSION["communicate_medium2"] = $_POST["communicate_medium2"];
    $_SESSION["communicate_medium3"] = $_POST["communicate_medium3"];

    if (sizeof($errors) != 0) {
        $_SESSION["error"] = $errors;
        header("Location:http://" . $_SERVER["SERVER_NAME"] . "/php/Registration_DB/registration_page.php");
        exit;
    }
     require_once 'db_conn.php';
    try
    {

        $conn->beginTransaction();
        $stmt = $conn->prepare("SELECT PK_id FROM state WHERE name=:residential_state");
        $stmt->bindParam(':residential_state', $_SESSION["residential_state"]);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($stmt->rowCount() > 0) {
            $r_state_id = $result['PK_id'];
        } else {
            $stmt = $conn->prepare("INSERT INTO state ( name,date_created ) VALUES ( :residential_state,NOW() )");
            $stmt->bindParam(':residential_state', $_SESSION["residential_state"]);
            $stmt->execute();
            $r_state_id = $conn->lastInsertId();
        }

        $stmt = $conn->prepare("SELECT PK_id FROM city WHERE name=:residential_city AND  FK_state_id=:residential_state_id");
        $stmt->bindParam(':residential_city', $_SESSION["residential_city"]);
        $stmt->bindParam(':residential_state_id', $r_state_id);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($stmt->rowCount() > 0) {
            $r_city_id = $result['PK_id'];
        } else {
            $stmt = $conn->prepare("INSERT INTO city ( name,FK_state_id,date_created )
            VALUES( :residential_city,:FK_state_id,NOW() )");
            $stmt->bindParam(':residential_city', $_SESSION["residential_city"]);
            $stmt->bindParam(':FK_state_id', $r_state_id);
            $stmt->execute();
            $r_city_id = $conn->lastInsertId();
        }

        $stmt = $conn->prepare("SELECT PK_id FROM state WHERE name=:office_state");
        $stmt->bindParam(':office_state', $_SESSION["office_state"]);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($stmt->rowCount() > 0) {
            $o_state_id = $result['PK_id'];
        } else {
            $stmt = $conn->prepare("INSERT INTO state ( name,date_created ) VALUES ( :office_state,NOW() )");
            $stmt->bindParam(':office_state', $_SESSION["office_state"]);
            $stmt->execute();
            $o_state_id = $conn->lastInsertId();
        }

        $stmt = $conn->prepare("SELECT PK_id FROM city WHERE name=:office_city AND FK_state_id=:office_state_id");
        $stmt->bindParam(':office_city', $_SESSION["office_city"]);
        $stmt->bindParam(':office_state_id', $o_state_id);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        echo $stmt->rowCount();

        if ($stmt->rowCount() > 0) {
            $o_city_id = $result['PK_id'];
        } else {
            $stmt = $conn->prepare("INSERT INTO city ( name,FK_state_id,date_created )
            VALUES ( :office_city,:FK_state_id,NOW() )");
            $stmt->bindParam(':office_city', $_SESSION["office_city"]);
            $stmt->bindParam(':FK_state_id', $o_state_id);
            $stmt->execute();
            $o_city_id = $conn->lastInsertId();
        }

        $stmt = $conn->prepare("SELECT PK_id FROM employment_type WHERE name=:title");
        $stmt->bindParam(':title', $_SESSION["title"]);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($stmt->rowCount() > 0) {
            $title_id = $result['PK_id'];
        } else {
            $stmt = $conn->prepare("INSERT INTO employment_type ( name,date_created ) VALUES ( :title,NOW() )");
            $stmt->bindParam(':title', $_SESSION["title"]);
            $stmt->execute();
            $title_id = $conn->lastInsertId();
        }

        $stmt = $conn->prepare("SELECT PK_id FROM employment_company WHERE name=:company_name");
        $stmt->bindParam(':company_name', $_SESSION["company_name"]);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($stmt->rowCount() > 0) {
            $company_name_id = $result['PK_id'];
        } else {
            $stmt = $conn->prepare("INSERT INTO employment_company ( name,date_created ) VALUES ( :company_name,NOW() )");
            $stmt->bindParam(':company_name', $_SESSION["company_name"]);
            $stmt->execute();
            $company_name_id = $conn->lastInsertId();
        }

        $stmt = $conn->prepare("SELECT * FROM employee WHERE email=:email");
        $stmt->bindParam(':email', $_SESSION["email"]);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($stmt->rowCount() == 0) {
            $stmt = $conn->prepare("INSERT INTO employee ( prefix,first_name,middle_name,last_name,gender,password,
            stackoverflow_id,FK_employment_type_id,FK_employment_company_id,marital_status,hash,fax,email,date_of_birth,date_created ) 
            VALUES ( :prefix,:first_name,:middle_name,:last_name,:gender,:password,:stackoverflow_id,:title_id,:company_name_id,:marital_status,
            :hash,:fax,:email,:date_of_birth,NOW() ) ");

            $stmt->bindParam(':prefix', $_SESSION["prefix"]);
            $stmt->bindParam(':first_name', $_SESSION["first_name"]);
            $stmt->bindParam(':middle_name', $_SESSION["middle_name"]);
            $stmt->bindParam(':last_name', $_SESSION["last_name"]);
            $stmt->bindParam(':gender', $_SESSION["gender"]);
            $stmt->bindParam(':password', md5($_SESSION["password"]));
            $stmt->bindParam(':stackoverflow_id',$_SESSION["stack_overflow_id"]);
            $stmt->bindParam(':title_id', $title_id);
            $stmt->bindParam(':company_name_id', $company_name_id);
            $stmt->bindParam(':marital_status', $_SESSION["marital_status"]);
            $stmt->bindParam(':hash', $hash);
            $stmt->bindParam(':fax', $_SESSION["fax"]);
            $stmt->bindParam(':email', $_SESSION["email"]);
            $stmt->bindParam(':date_of_birth', $_SESSION["date_of_birth"]);
            $stmt->execute();
            $employee_id = $conn->lastInsertId();
            //Creating hash for email verification using first name,last name and email
            $hash = md5 ( $_SESSION['first_name'] . $_SESSION['last_name'] . $_SESSION['email'] . $employee_id . 'verify');
        } else {
            $_SESSION["error"] = "Employee Exist";
            $conn->rollBack();
            header("Location:http://" . $_SERVER["SERVER_NAME"] . "/php/Registration_DB/registration_page.php");
        }

        $stmt = $conn->prepare("INSERT INTO address ( zip_code,street,FK_city_id,type,FK_employee_id,date_created )
         VALUES ( :residential_zip_code,:residential_address,:r_city_id,:type,:employee_id,NOW() )");
        $stmt->bindParam(':residential_zip_code', $_SESSION["residential_zip_code"]);
        $stmt->bindParam(':residential_address', $_SESSION["residential_address"]);
        $stmt->bindParam(':r_city_id', $r_city_id);
        $stmt->bindValue(':type', 'Residential');
        $stmt->bindParam(':employee_id', $employee_id);
        $stmt->execute();
        $stmt = $conn->prepare("INSERT INTO address ( zip_code,street,FK_city_id,type,FK_employee_id,date_created )
            VALUES ( :office_zip_code,:office_address,:o_city_id,:type,:employee_id,NOW() )");
        $stmt->bindParam(':office_zip_code', $_SESSION["office_zip_code"]);
        $stmt->bindParam(':office_address', $_SESSION["office_address"]);
        $stmt->bindParam(':o_city_id', $o_city_id);
        $stmt->bindValue(':type', 'Official');
        $stmt->bindParam(':employee_id', $employee_id);
        $stmt->execute();
        $stmt = $conn->prepare("INSERT INTO contact ( type,FK_employee_id,numbers,date_created )
         VALUES ( :type,:employee_id,:telephone,NOW() )");
        $stmt->bindParam(':employee_id', $employee_id);
        $stmt->bindParam(':telephone', $_SESSION["telephone"]);
        $stmt->bindValue(':type', 'Telephone');
        $stmt->execute();
        $stmt = $conn->prepare("INSERT INTO contact ( type,FK_employee_id,numbers,date_created )
         VALUES ( :type,:employee_id,:mobile,NOW() )");
        $stmt->bindParam(':employee_id', $employee_id);
        $stmt->bindParam(':mobile', $_SESSION["mobile"]);
        $stmt->bindValue(':type', 'Mobile');
        $stmt->execute();

        if ($_SESSION["communicate_medium1"] != "") {
            $stmt = $conn->prepare("INSERT INTO employee_communication ( FK_employee_id,FK_communication_type_id )
                VALUES ( :employee_id,'1' )");
            $stmt->bindParam(':employee_id', $employee_id);
            $stmt->execute();
        }

        if ($_SESSION["communicate_medium2"] != "") {
            $stmt = $conn->prepare("INSERT INTO employee_communication ( FK_employee_id,FK_communication_type_id )
                VALUES ( :employee_id,'2' )");
            $stmt->bindParam(':employee_id', $employee_id);
            $stmt->execute();
        }

        if ($_SESSION["communicate_medium3"] != "") {
            $stmt = $conn->prepare("INSERT INTO employee_communication ( FK_employee_id,FK_communication_type_id )
                VALUES ( :employee_id,'3' ) ");
            $stmt->bindParam(':employee_id', $employee_id);
            $stmt->execute();
        }
        $conn->commit();

        //Creating verification link
        $verify_link = "http://".$_SERVER['SERVER_NAME']."/php/Registration_DB/verify_email.php?email=".$_SESSION['email']."&&hash=".$hash;
        
        $messgae = "<h4>Thank for registering to profile app.</h4><br>
                    <b>User id :</b>" . $_SESSION['email'] . "<br>
                    <b>Verification link :</b><a href=" . $verify_link . ">" . $verify_link."</a>
                    <br>Regards,<br>Profile App team";
        
        mail($_SESSION['email'],"Verify the account",$messgae,"From: Admin <mindfireishan@gmail.com>"."\r\n"."Content-type: text/html; charset=>utf-8");

        header("Location:http://" . $_SERVER["SERVER_NAME"] . "/php/Registration_DB/registration_successful.php");

    } catch (exception $e) {
        logError($e->getMessage());
        $conn->rollBack();
        echo "Statement Error";

    }
