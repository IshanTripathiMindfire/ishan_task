<?php
	/*
	 * Update DB with modified data after validation
	 * */
	try {

		$errors = array();

		include 'functions.php';

		check_regular_expression($_POST["update_r_state"], "Residential state should contain only alphabets.", 0);
		check_regular_expression($_POST["update_r_city"], "Residential city  should contain only alphabets.", 1);
		check_regular_expression_number($_POST["update_r_zip_code"], "Residential zip code should contain only numbers.", 2);
		check_regular_expression($_POST["update_o_state"], "Official state should contain only alphabets.", 3);
		check_regular_expression($_POST["update_o_city"], "Official city  should contain only alphabets.", 4);
		check_regular_expression_number($_POST["update_o_zip_code"], "Official zip code should contain only numbers.", 5);
		check_regular_expression_number($_POST["update_telephone"], "Telephone should contain only numbers.", 6);
		check_regular_expression_number($_POST["update_mobile"], "Mobile should contain only numbers.", 7);
		check_regular_expression($_POST["update_designation"], "Designation should contain only alphabets.", 8);
		check_regular_expression($_POST["update_company"], "Company name should contain only alphabets.", 9);
		check_regular_expression_number($_POST["update_stackoverflow_id"],"Stack overflow id should be number",10);
		print_r($errors);
		if (sizeof($errors) <= 0) {

			require_once 'db_conn.php';
			$conn->beginTransaction();

			$stmt = $conn->prepare("SELECT PK_id FROM employment_type WHERE name=:title");
			$stmt->bindParam(':title', $_POST["update_designation"]);
			$stmt->execute();
			$result = $stmt->fetch(PDO::FETCH_ASSOC);
			if ($stmt->rowCount() > 0) {
				$designation_id = $result['PK_id'];
			} else {
				$stmt = $conn->prepare("INSERT INTO employment_type ( name,date_created ) VALUES ( :title,NOW() ) ");
				$stmt->bindParam(':title', $_POST["update_designation"]);
				$stmt->execute();
				$title_id = $conn->lastInsertId();
			}

			$stmt = $conn->prepare("SELECT PK_id FROM employment_company WHERE name=:company_name");
			$stmt->bindParam(':company_name', $_POPST["update_company"]);
			$stmt->execute();
			$result = $stmt->fetch(PDO::FETCH_ASSOC);

			if ($stmt->rowCount() > 0) {
				$company_name_id = $result['PK_id'];
			} else {
				$stmt = $conn->prepare("INSERT INTO employment_company ( name,date_created ) VALUES ( :company_name,NOW() )");
				$stmt->bindParam(':company_name', $_POST["update_company"]);
				$stmt->execute();
				$company_name_id = $conn->lastInsertId();
			}

			$stmt = $conn->prepare("UPDATE employee SET  password = :password, date_of_birth = :date_of_birth, 
				stackoverflow_id = :stackoverflow_id,FK_employment_type_id =  :designation_id, 
				FK_employment_company_id =  :company_name_id WHERE Pk_id = :id");
			$stmt->bindParam(':password', $_POST["update_password"]);
			$stmt->bindParam(':date_of_birth', $_POST["update_dob"]);
			$stmt->bindParam(':stackoverflow_id',$_POST["update_stackoverflow_id"]);
			$stmt->bindParam(':designation_id', $designation_id);
			$stmt->bindParam(':company_name_id', $company_name_id);
			$stmt->bindParam(':id', $_POST["update_id"]);
			$stmt->execute();

			$stmt = $conn->prepare("SELECT PK_id FROM state WHERE name = :residential_state");
			$stmt->bindParam(':residential_state', $_POST["update_r_state"]);
			$stmt->execute();
			$result = $stmt->fetch(PDO::FETCH_ASSOC);
			if ($stmt->rowCount() > 0) {
				$r_state_id = $result['PK_id'];
			} else {
				$stmt = $conn->prepare("INSERT INTO state ( name,date_created ) VALUES ( :residential_state,NOW() )");
				$stmt->bindParam(':residential_state', $_POST["update_r_state"]);
				$stmt->execute();
				$r_state_id = $conn->lastInsertId();
			}

			$stmt = $conn->prepare("SELECT PK_id FROM city WHERE name = :residential_city AND FK_state_id = :r_state_id");
			$stmt->bindParam(':residential_city', $_POST["update_r_city"]);
			$stmt->bindParam(':r_state_id', $r_state_id);
			$stmt->execute();
			$result = $stmt->fetch(PDO::FETCH_ASSOC);

			if ($stmt->rowCount() > 0) {
				$r_city_id = $result['PK_id'];
			} else {
				$stmt = $conn->prepare("INSERT INTO city ( name,FK_state_id,date_created )
				VALUES ( :residential_city,:FK_state_id,NOW() )");
				$stmt->bindParam(':residential_city', $_POST["update_r_city"]);
				$stmt->bindParam(':FK_state_id', $r_state_id);
				$stmt->execute();
				$r_city_id = $conn->lastInsertId();
			}

			$stmt = $conn->prepare("SELECT PK_id FROM state WHERE name = :official_state");
			$stmt->bindParam(':official_state', $_POST["update_o_state"]);
			$stmt->execute();
			$result = $stmt->fetch(PDO::FETCH_ASSOC);
			if ($stmt->rowCount() > 0) {
				$o_state_id = $result['PK_id'];
			} else {
				$stmt = $conn->prepare("INSERT INTO state ( name,date_created ) VALUES ( :official_state,NOW() )");
				$stmt->bindParam(':official_state', $_POST["update_o_state"]);
				$stmt->execute();
				$o_state_id = $conn->lastInsertId();
			}

			$stmt = $conn->prepare("SELECT PK_id FROM city WHERE name = :official_city AND FK_state_id = :o_state_id");
			$stmt->bindParam(':official_city', $_POST["update_o_city"]);
			$stmt->bindParam(':o_state_id', $o_state_id);
			$stmt->execute();
			$result = $stmt->fetch(PDO::FETCH_ASSOC);

			if ($stmt->rowCount() > 0) {
				$o_city_id = $result['PK_id'];
			} else {
				$stmt = $conn->prepare("INSERT INTO city ( name,FK_state_id,date_created )
				VALUES ( :official_city,:FK_state_id,NOW() )");
				$stmt->bindParam(':official_city', $_POST["update_o_city"]);
				$stmt->bindParam(':FK_state_id', $o_state_id);
				$stmt->execute();
				$o_city_id = $conn->lastInsertId();
			}

			$stmt = $conn->prepare("UPDATE address SET  street = :street, zip_code = :zip_code, FK_city_id = :r_city_id
			WHERE FK_employee_id = :id AND type = 'Residential'");
			$stmt->bindParam(':street', $_POST["update_r_street"]);
			$stmt->bindParam(':zip_code', $_POST["update_r_zip_code"]);
			$stmt->bindParam(':r_city_id', $r_city_id);
			$stmt->bindParam(':id', $_POST["update_id"]);
			$stmt->execute();

			$stmt = $conn->prepare("UPDATE address SET  street = :street, zip_code = :zip_code, FK_city_id = :o_city_id
			WHERE FK_employee_id = :id AND type = 'Official'");
			$stmt->bindParam(':street', $_POST["update_o_street"]);
			$stmt->bindParam(':zip_code', $_POST["update_o_zip_code"]);
			$stmt->bindParam(':o_city_id', $o_city_id);
			$stmt->bindParam(':id', $_POST["update_id"]);
			$stmt->execute();

			$stmt = $conn->prepare("UPDATE contact SET  numbers = :number WHERE FK_employee_id = :id AND type = 'Telephone'");
			$stmt->bindParam(':number', $_POST["update_telephone"]);
			$stmt->bindParam(':id', $_POST["update_id"]);
			$stmt->execute();

			$stmt = $conn->prepare("UPDATE contact SET  numbers = :number
			WHERE FK_employee_id = :id AND type = 'Mobile'");
			$stmt->bindParam(':number', $_POST["update_mobile"]);
			$stmt->bindParam(':id', $_POST["update_id"]);
			$stmt->execute();

			$conn->commit();
		} else {
			echo json_encode($errors);
		}
	} catch (exception $e) {
		$conn->rollBack();
        logError($e->getMessage());
        echo "Internal Error update fail";
	}

