<?php
/*
 * Registration page layout
*/
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Registration Page</title>
        <?php include 'include.php'; ?>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>

    <body >
        <div class="container form-group">

            <form action="registration_data.php" method="POST" id="registration_form">

                <div class="col-lg-12 form-group">
                    <nav role="navigation" class="navbar navbar-default">
                        <div class="navbar-header">
                            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" aria-expanded="false" aria-controls="navbarCollapse" class="navbar-toggle">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a href="#" class="navbar-brand">Registration</a>
                        </div>
                        <div id="navbarCollapse" class="collapse navbar-collapse ">
                            <ul class="nav navbar-nav">
                                <li><a href="./index.php">Login</a></li>
                            </ul>
                        </div>
                    </nav>

                    <?php
                    session_start();
                        if (sizeof($_SESSION["error"]) > 0) {
                                echo "<pre><h4 class='alert-danger'>";
                                print_r($_SESSION["error"]);
                                echo "</h4></pre>";
                        }
                    ?>
                       <fieldset class="scheduler-border">
                           <div>
                               <legend> Your Basic Profile</legend>
                               <div class="row ">

                                   <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                        <span id="prefix"><label>Prefix:</label></span>
                                        <span id="prefix1"><label for="prefix_input1"><input type="radio" name="prefix_input"
                                        id="prefix_input1" value="Mr." checked>Mr.</input></label></span>
                                        <span id="prefix2"><label for="prefix_input2"><input type="radio" name="prefix_input"
                                         id="prefix_input2" value="Mrs." >Mrs.</input></label></span>
                                        <span id="prefix3"><label for="prefix_input3"><input type="radio" name="prefix_input"
                                        id="prefix_input3" value="Ms." >Ms.</input></label></span>
                                   </div>
                               </div>
                               <div class="row">
                                   <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 ">
                                        <span id="name">
                                         <label>First Name:</label>
                                        <input type="text" name="first_name" placeholder="Enter first name here.."
                                        id="f_name"  class="form-control" value = "<?php echo isset($_SESSION["first_name"]) ?
                                        $_SESSION["first_name"] : ""; ?>" >
                                        </span>
                                   </div>

                                   <div class=" col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 ">
                                        <span id="name">
                                        <label>Middle Name:</label>
                                        <input type="text" name="middle_name" placeholder="Enter middle name here.." id="m_name"
                                        class="form-control" value = "<?php echo isset($_SESSION["middle_name"]) ?
                                        $_SESSION["middle_name"] : ""; ?>">
                                        </span>
                                   </div>

                                   <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
                                        <span id="name">
                                        <label>Last Name:</label>
                                        <input type="text" name="last_name" placeholder="Enter last name here.."
                                        id="l_name" class="form-control" value = "<?php echo isset($_SESSION["last_name"]) ?
                                        $_SESSION["last_name"] : ""; ?>">
                                        </span>
                                   </div>
                               </div>
                               <div class="row">
                                   <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 hidden-xs ">
                                        <span id="gender"><label>Gender:</label></span>
                                        <span id="gender-male"><label for="gender_male"><input type="radio" name="gender"
                                        id="gender_male" class="form-inline" value="Male" checked >Male</input></label></span>
                                        <span id="gender-female"><label for="gender_female"><input type="radio" class="form-inline"
                                        name="gender" value="Female" id="gender_female" >Female</input></label></span>
                                   </div>

                                   <div class="col-xs-12 visible-xs">
                                        <span id="gender"><label>Gender:</label></span></div>
                                        <div class="col-xs-12 visible-xs">
                                        <span id="gender-male"><label for="gender_male"><input type="radio" name="gender"
                                        class="form-inline" id="gender_male" value="Male"  >Male</input></label></span>
                                        <span id="gender-female"><label for="gender_female"><input type="radio" name="gender"
                                       class="form-inline" value="Female" id="gender_female" >Female</input></label></span>
                                   </div>

                                   <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 hidden-xs">
                                        <span id="ms"><label>Marital Status:</label></span>
                                        <span id="marital_status1"><label for="single"><input type="radio" class="form-inline"
                                        name="marital_status" id="single" value="Single" checked>Single</input></label></span>
                                        <span id="marital_status2"><label for="married"><input type="radio" class="form-inline"
                                        name="marital_status" id="married" value="Married" >Married</input></label></span>
                                        <span id="marital_status3"><label for="divorce"><input type="radio" class="form-inline"
                                        name="marital_status" id="divorce" value="Divorce" >Divorce</input></label></span>
                                    </div>
                                   <div class="col-xs-12 visible-xs">
                                        <span id="ms"><label>Marital Status:</label></span></div>
                                        <div class="col-xs-12 visible-xs">
                                        <span id="marital_status1"><label for="single"><input type="radio" name="marital_status"
                                        id="single" value="Single" >Single</input></label></span>
                                        <span id="marital_status2"><label for="married"><input type="radio" name="marital_status"
                                        id="married" value="Married" >Married</input></label></span>
                                        <span id="marital_status3"><label for="divorce"><input type="radio" name="marital_status"
                                        id="divorce" value="Divorce" >Divorce</input></label></span>
                                    </div>
                               </div>
                               <div class="row">
                                   <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                                       <span id="date_of_birth"><label>Date of Birth:</label>
                                        <input type="Date" class="form-control" name="date_of_birth"
                                        id="dob" placeholder="YYYY/MM/DD"
                                        value = "<?php echo isset($_SESSION["date_of_birth"]) ? $_SESSION["date_of_birth"] : ""; ?>">
                                   </div>
                                   <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                                        <span id="stack_overflow"><label>Stack Overflow Id:</label>
                                        <input type="text" class="form-control" name="stack_overflow_id"
                                        id="stack_overflow_id" placeholder="Enter  your stack overflow id here"
                                        value = "<?php echo isset($_SESSION["stack_overflow_id"]) ? $_SESSION["stack_overflow_id"] : ""; ?>">
                                        </span>
                                   </div>
                               </div>
                           </div>
                       </fieldset>
                       <fieldset class="scheduler-border">
                            <div>
                               <legend> Communication Profile</legend>
                               <div class="row">
                                   <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                                        <span id="telephone">
                                            <label>Telphone:</label>
                                            <input type="text" name="telephone_input" id="telephone_input"
                                            placeholder="Enter telephone number here.." class="form-control"
                                            value = "<?php echo isset($_SESSION["telephone"]) ? $_SESSION["telephone"] : ""; ?>" >
                                        </span>
                                   </div>
                                   <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                                        <span id="mobile">
                                            <label>Mobile:</label>
                                            <input type="text" name="mobile" id="mobile_input" placeholder="Enter the mobile number here.."
                                            class="form-control" value = "<?php echo isset($_SESSION["mobile"]) ? $_SESSION["mobile"] : ""; ?>">
                                        </span>
                                   </div>
                               </div>
                               <div class="row">
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                                        <span id="email">
                                        <label>Email:</label>
                                        <input type="email" name="email" id="email_input"
                                        placeholder="Enter email id here.." class="form-control"
                                        value = "<?php echo isset($_SESSION["email"]) ? $_SESSION["email"] : ""; ?>">
                                        </span>
                                    </div>

                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                                        <span id="fax">
                                        <label>Fax</label><input type="text" name="fax" id="fax_input"
                                        placeholder="Enter fax number here.." class="form-control"
                                        value ="<?php echo isset($_SESSION["fax"]) ? $_SESSION["fax"] : ""; ?>">
                                        </span>
                                    </div>
                               </div>
                               <div class="row">
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                                        <span id="password">
                                        <label>Password:</label>
                                        <input type="password" name="password" id="i_password" placeholder="Enter password here"
                                        class="form-control"  />
                                        </span>
                                    </div>

                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                                        <span id="confirm_password">
                                        <label>Confirm Password:</label>
                                        <input type="password" name="confirm_password" id="c_password"
                                        placeholder="Enter password here" class="form-control"/>
                                        </span>
                                    </div>
                               </div>
                               <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                        <span id="residential_address_input">
                                        <label>Residential Address:</label>
                                        <textarea name="residential_address_input" placeholder="Enter residential address here.."
                                        rows="4" cols="4"
                                        class="form-control"  value = "<?php echo isset($_SESSION["residential_address"]) ?
                                            $_SESSION["residential_address"] : ""; ?>"></textarea>
                                        </span>
                                    </div>
                               </div>
                               <div class="row">
                                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
                                        <span id="city">
                                        <label>City:</label>
                                        <input type="text" name="residential_city" placeholder="Enter city name here.."
                                        id="r_city" class="form-control"
                                        value = "<?php echo isset($_SESSION["residential_city"]) ?
                                        $_SESSION["residential_city"] : ""; ?>">
                                        </span>
                                    </div>

                                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
                                        <span id="state">
                                        <label>State:</label>
                                        <input type="text" name="residential_state" placeholder="Enter state here.."
                                        id="state_input" class="form-control"
                                        value = "<?php echo isset($_SESSION["residential_state"]) ?
                                        $_SESSION["residential_state"] : ""; ?>">
                                        </span>
                                    </div>

                                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
                                        <span id="zip_code">
                                        <label>Zip-Code:</label>
                                        <input type="text" name="residential_zip_code" placeholder="Enter zip code here.."
                                        id="r_zip_code" class="form-control"
                                        value = "<?php echo isset($_SESSION["residential_zip_code"]) ?
                                        $_SESSION["residential_zip_code"] : ""; ?>">
                                        </span>
                                    </div>
                               </div>
                            </div>
                       </fieldset>
                       <fieldset class="scheduler-border">
                           <div>
                                 <legend > Corporate Profile</legend>
                                 <div class="row">
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                                        <span id="title">
                                        <label>Designation:</label>
                                        <input type="text" name="title" placeholder="Enter the title here.."
                                        id="title_input" class="form-control"
                                        value = "<?php echo isset($_SESSION["title"]) ? $_SESSION["title"] : ""; ?>">
                                        </span>
                                     </div>
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                                        <span id="company_name">
                                        <label>Company:</label>
                                        <input type="text" name="company_name" placeholder="Enter the company here.."
                                        id="company" class="form-control"
                                        value = "<?php echo isset($_SESSION["company_name"]) ? $_SESSION["company_name"] : ""; ?>">
                                        </span>
                                    </div>
                                </div>
                               <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                        <span id="office_address_input">
                                        <label>Office Address:</label>
                                        <textarea name="office_address_input"
                                        placeholder="Enter office address here.." rows="4" cols="4"
                                        class="form-control"  value = "<?php echo isset($_SESSION["office_address"]) ?
                                        $_SESSION["office_address"] : ""; ?>"></textarea>
                                        </span>
                                    </div>
                               </div>
                               <div class="row">
                                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
                                        <span id="city">
                                        <label>City:</label>
                                        <input type="text" name="office_city" placeholder="Enter city name here.."
                                        id="o_city" class="form-control"
                                        value = "<?php echo isset($_SESSION["office_city"]) ? $_SESSION["office_city"] : ""; ?>">
                                        </span>
                                    </div>

                                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
                                        <span id="state">
                                        <label>State:</label>
                                        <input type="text" name="office_state" placeholder="Enter state here.." id="o_state"
                                        class="form-control"  value = "<?php echo isset($_SESSION["office_state"]) ?
                                        $_SESSION["office_state"] : ""; ?>">
                                        </span>
                                    </div>

                                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
                                        <span id="zip_code">
                                        <label>Zip-Code:</label>
                                        <input type="text" name="office_zip_code" placeholder="Enter zip code here.." id="o_zip_code"
                                        class="form-control" value = "<?php echo isset($_SESSION["office_zip_code"]) ?
                                        $_SESSION["office_zip_code"] : ""; ?>">
                                        </span>
                                    </div>
                               </div>
                           </div>
                       </fieldset>
                       <fieldset class="scheduler-border">
                            <div class="row">
                               <div class="col-xl-12 col-lg-12 col-md-12  hidden-xs ">
                                    <span><label>Communicate Medium:</label></span>
                                    <span id="communicate_through"><label for="SMS">
                                    <input type="checkbox" name="communicate_medium1" id="SMS" value="SMS" checked>SMS</input></label>
                                    </span>
                                    <span id="communicate_through"><label for="Call">
                                    <input type="checkbox" name="communicate_medium2" id="Call" value="Call" checked>Call</input></label>
                                    </span>
                                    <span id="communicate_through"><label for="Email">
                                    <input type="checkbox" name="communicate_medium3" id="Email" value="Email" checked>Email</input>
                                    </label></span>
                               </div>
                               <div class=" col-xs-12  visible-xs">
                                    <span><label>Communicate Medium:</label></span></div>

                                    <div class=" col-xs-12  visible-xs">
                                     <span id="communicate_through"><label for="SMS">
                                    <input type="checkbox" name="communicate_medium1" id="SMS" value="SMS" checked>SMS</input></label>
                                    </span>
                                    <span id="communicate_through"><label for="Call">
                                    <input type="checkbox" name="communicate_medium2" id="Call" value="Call" checked >Call</input></label>
                                    </span>
                                    <span id="communicate_through"><label for="Email">
                                    <input type="checkbox" name="communicate_medium3" id="Email" value="Email" checked >Email</input>
                                    </label></span>
                               </div>
                            </div>
                           <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                <input type="submit" name="submit" id="submit" class="btn-primary btn-lg" value="Register"/>
                           </div>
                       </fieldset>
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>
