<?php
/*
 * Dashboard page file and validation for input login details
 */
?>
<!DOCTYPE html>
<html>

    <head>
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <?php include 'include.php'; ?>
    	<title> Dashboard Page </title>
    </head>

    <body>

    <?php
    session_start();
    if($_SESSION['error'] != 'already_logged_in') {
        header("Location:http://" . $_SERVER["SERVER_NAME"] . "/php/Registration_DB/index.php");
    }

    try {

        require_once 'db_conn.php';

        echo '  <nav role="navigation" class="navbar navbar-default">
        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse"
            data-toggle="collapse" class="navbar-toggle">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand">Dashboard</a>
        </div>
        <div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#">Profile</a></li>
            <li><a href="#">Messages</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
            <li><a href="/php/Registration_DB/logout.php">Logout</a></li>
            </ul>
        </div>
     </nav>';

    echo "<h1>Welcome to dashboard :" . $_SESSION["name"] . "</h1>";

    echo '<h2> Email id:' . $_SESSION["email_id"] . '</h2>';

    $query = 'Select e.PK_id,concat(prefix," ",first_name," ",middle_name," ",last_name) as name,email,stackoverflow_id,
                        concat(a1.street," ",cn1.name," ",s1.name," ",a1.zip_code) as residential_address,
                        concat(a2.street," ",cn2.name," ",s2.name," ",a2.zip_code) as official_address,
                        c1.numbers as telephone,
                        c2.numbers as mobile,
                        et.name as designation,
                        ec.name as company
                        from employee e
                        left join address a1 on e.PK_id=a1.FK_employee_id AND a1.type="Residential"
                        left join address a2 on e.PK_id=a2.FK_employee_id AND a2.type="Official"
                        left join contact c1 on e.PK_id=c1.FK_employee_id AND c1.type="Telephone"
                        left join contact c2 on e.PK_id=c2.FK_employee_id AND c2.type="Mobile"
                        left join employment_type et on et.PK_id=e.FK_employment_type_id
                        left join employment_company ec on ec.PK_id=e.FK_employment_company_id
                        left join city cn1 on cn1.PK_id=a1.FK_city_id AND a1.type="Residential"
                        left join city cn2 on cn2.PK_id=a2.FK_city_id AND a2.type="Official"
                        left join state s1 on s1.PK_id=cn1.FK_state_id AND a1.type="Residential"
                        left join state s2 on s2.PK_id=cn2.FK_state_id AND a2.type="Official"
                        group by e.PK_id';
    $stmt = $conn->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll();

    echo "<div class='table-responsive display' style='margin:2%;'>";
    echo "<table class='table-bordered table-hover  table-condensed' id='data'>";
    echo "<thead><tr>
                <th>Name</th>
                <th>Residential Address</th>
                <th>Telephone</th>
                <th>Mobile</th>
                <th>Email</th>
                <th>Designation</th>
                <th>Company</th>
                <th>Official Address</th>
                <th>Stack Overflow Id</th>
                </tr></thead><tbody>";

    foreach ($result as $x) {
        echo "<tr id=$x[PK_id]>";
        echo "<td>$x[name]</td>
                    <td>$x[residential_address]</td>
                    <td>$x[telephone]</td>
                    <td>$x[mobile]</td>
                    <td>$x[email]</td>
                    <td>$x[designation]</td>
                    <td>$x[company]</td>
                    <td>$x[official_address]</td>
                    <td>$x[stackoverflow_id]</td>
                    ";
        echo "</tr>";
    }
    echo "</tbody></table></div>";

} catch (exception $e) {
    echo $e->getMessage();
}
?>

        <div id="modal" class="modal fade " role="dialog">
            <div class="modal-dialog ">
                <div class="modal-content">
                    <div id="loader" class="loader hidden "></div>
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Employee Details</h4>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id = "update" >Update</button>
                        <button type="button" class="btn btn-default" id = "delete" >Delete</button>
                        <button type="button" class="btn btn-default" id = "stack" >Stack Overflow</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>


        <div id="modal_update" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form >
                        <div class="modal-header form-group">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Update Employee Details</h4>
                        </div>
                        <div class="modal-body form-group" id="modal-body-update">
                        </div>
                        <div class="modal-footer form-group">
                            <button type="submit" class="btn btn-default" id="update_db" >Update</button>
                            <button type="button" class="btn btn-default" id = "close_update" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div id="modal_delete" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form >
                        <div class="modal-header form-group">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Delete Employee Details</h4>
                        </div>
                        <div class="modal-body form-group" id="modal-body-delete">
                        </div>
                        <div class="modal-footer form-group">
                            <button type="submit" class="btn btn-default" id="delete_db" >Delete</button>
                            <button type="button" class="btn btn-default" id="close_delete" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div id="modal_stack" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form >
                        <div class="modal-header form-group">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Stack Overflow Employee Details</h4>
                        </div>
                        <div class="modal-body form-group" id="modal-body-stackoverflow">
                        </div>
                        <div class="modal-footer form-group">
                            <button type="button" class="btn btn-default" id="close_stack" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>