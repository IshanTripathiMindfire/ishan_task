<?php

require_once 'db_conn.php';
try {
    $stmt = $conn->prepare("SELECT hash FROM employee WHERE email=:email");
    $stmt->bindParam(':email',$_GET['email']);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    if($result['hash'] == $_GET['hash']) {
        $stmt = $conn->prepare("UPDATE employee SET verify_status = 1 WHERE email=:email");
        $stmt->bindParam(':email', $_GET['email']);
        $stmt->execute();
    }
    header("Location:http://" . $_SERVER["SERVER_NAME"] . "/php/Registration_DB/index.php");
} catch (exception $e) {
    echo $e->getMessage();
}

