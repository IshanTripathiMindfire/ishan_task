// This comment code is for later use
/*function check(){
  
  var password=document.getElementById("i_password").value;
  var c_password=document.getElementById("c_password").value;
  
  if (password != c_password){
    
    document.getElementById("error_show").innerHTML="Password does not match";
  }
  else
  {
    
    document.getElementById("error_show").innerHTML="";
  }
}

function pattern_check(field_id,field_pattern,message)
{
  if (!(field_pattern.test(document.getElementById(field_id).value)) )
  {
    $(("#"+field_id)).parent().addClass("has-error text-danger ");
   
    //document.getElementById(field_id).dataset.data-toggle = "popover";
    //document.getElementById(field_id).dataset.data-content = "Some content inside the popover";
    document.getElementById(field_id).setAttribute("data-toggle","popover");
    document.getElementById(field_id).setAttribute("data-content",message);
    $(("#"+field_id)).popover('toggle'); 
    } 
  else
  {
    $(("#"+field_id)).parent().removeClass("has-error text-danger");
    document.getElementById(field_id).removeAttribute("data-toggle");
    document.getElementById(field_id).removeAttribute("data-content");
    $(("#"+field_id)).popover('hide');
    
  }
}*/

//----------------------------------------------------------------------------------------------------------------------

$(document).ready(function() {
    $('#data').DataTable();
});

var onlyLetterRegExp = /^[a-z\s]*$/i;
var emailRegExp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
var onlyNumberRegExp = /^[0-9]*$/i;

 $(document).ready(function() {
     $("#registration_form").submit(function() {
         $('.error').remove();
         var error = [];
         if ($('input[name = prefix_input]:checked').length != 1)
         {
             $('#prefix').parent().addClass("has-error text-danger ");
             $('#prefix3').after("<div class='error'><b>Select one prefix</b></div>");
             error[0] = 1;
         }
         else
         {
             $('#prefix').parent().removeClass("has-error text-danger ");
             $('#prefix_input3').next("div").remove();
             error[0] = 0;
         }

         if ($('#f_name').val().trim() == "" || ! (onlyLetterRegExp.test($('#f_name').val().trim())))
         {
            $('#f_name').parent().addClass("has-error text-danger ");
            $('#f_name').after("<div class='error'><b>First name is required & only alphabet allowed</b></div>");
            error[1] = 1;
         }
         else
         {
            $('#f_name').parent().removeClass("has-error text-danger ");
            $('#f_name').next("div").remove();
            error[1] = 0;
         }

         if ( ! (onlyLetterRegExp.test($('#m_name').val().trim())))
         {
            $('#m_name').parent().addClass("has-error text-danger ");
            $('#m_name').after("<div class='error'><b>Middle name is required & only alphabet allowed</b></div>");
            error[2] = 1;
         }
         else if ($("#m_name").val().trim() != "" && (onlyLetterRegExp.test($("#m_name").val().trim())))
         {
            $('#m_name').parent().removeClass("has-error text-danger ");
            $('#m_name').next("div").remove();
            error[2] = 0;
         }

     
         if ($("#l_name").val().trim() == "" || ! (onlyLetterRegExp.test($("#l_name").val().trim())))
         {
            $('#l_name').parent().addClass("has-error text-danger ");
            $('#l_name').after("<div class='error'><b>Last name is required & only alphabet allowed</b></div>");
            error[3] = 1;
         }
         else
         {
            $('#l_name').parent().removeClass("has-error text-danger ");
            $('#l_name').next("div").remove();
            error[3] = 0;
         }
         if ($('input[name = gender]:checked').length != 1)
         {
             $('#gender').parent().addClass("has-error text-danger ");
             $('#gender-female').after("<div class='error'><b>Select one gender</b></div>");
             error[4] = 1;
         }
         else
         {
             $('#gender').parent().removeClass("has-error text-danger ");
             $('#gender-female').next("div").remove();
             error[4] = 0;
         }

         if ($('input[name = marital_status]:checked').length != 1)
         {
             $('#ms').parent().addClass("has-error text-danger ");
             $('#marital_status3').after("<div class='error'><b>Select one marital status</b></div>");
             error[5] = 1;
         }
         else
         {
             $('#ms').parent().removeClass("has-error text-danger ");
             $('#marital_status3').next("div").remove();
             error[5] = 0;
         }
     
         if ($("#dob").val() == "")
         {
            $('#dob').parent().addClass("has-error text-danger ");
            $('#dob').after("<div class='error'><b>Date of birth required</b></div>");
            error[6] = 1;
         }
         else if ($("#dob").val() != "")
         {
            $('#dob').parent().removeClass("has-error text-danger ");
            $('#dob').next("div").remove();
            error[6] = 0;
         }

         if ( ! (onlyNumberRegExp.test($("#stack_overflow_id").val().trim())))
         {
             $('#stack_overflow_id').parent().addClass("has-error text-danger ");
             $('#stack_overflow_id').after("<div class='error'><b>Stackoverflow id should contain only numbers</b></div>");
             error[7] = 1;
         }
         else if ($("#stack_overflow_id").val().trim() != "" && (onlyNumberRegExp.test($("#stack_overflow_id").val().trim())))
         {
            $('#stack_overflow_id').parent().removeClass("has-error text-danger ");
            $('#stack_overflow_id').next("div").remove();
            error[7] = 0;
         }

         if ($("#telephone_input").val().trim() == "" || ! (onlyNumberRegExp.test($("#telephone_input").val().trim())))
         {
            $('#telephone_input').parent().addClass("has-error text-danger ");
            $('#telephone_input').after("<div class='error'><b>Telephone number is required & numeric only</b></div>");
            error[8] = 1;
         }
         else
         {
            $('#telephone_input').parent().removeClass("has-error text-danger ");
            $('#telephone_input').next("div").remove();
            error[8] = 0;
         }
     
         if ($("#mobile_input").val().trim() == "" || ! (onlyNumberRegExp.test($("#mobile_input").val().trim())))
         {
            $('#mobile_input').parent().addClass("has-error text-danger ");
            $('#mobile_input').after("<div class='error'><b>Mobile number is required & numeric only</b></div>");
            error[9] = 1;
         }
         else
         {
            $('#mobile_input').parent().removeClass("has-error text-danger ");
            $('#mobile_input').next("div").remove();
            error[9] = 0;
         }
     
         if ($("#email_input").val().trim() == "" || ! (emailRegExp.test($("#email_input").val().trim())))
         {
            $('#email_input').parent().addClass("has-error text-danger ");
            $('#email_input').after("<div class='error'><b>Correct email id required </b></div>");
            error[10] = 1;
         }
         else if ($("#email_input").val().trim() != "" && (emailRegExp.test($("#email_input").val().trim())))
         {
            $('#email_input').parent().removeClass("has-error text-danger ");
            $('#email_input').next("div").remove();
            error[10] = 0;
         }
     
         if ( ! (onlyNumberRegExp.test($("#fax_input").val().trim())))
         {
            $('#fax_input').parent().addClass("has-error text-danger ");
            $('#fax_input').after("<div class='error'><b>Fax number should contain only numbers </b></div>");
            error[11] = 1;
         }
         else if ($("#fax_input").val().trim() != "" && (onlyNumberRegExp.test($("#fax_input").val().trim())))
         {
            $('#fax_input').parent().removeClass("has-error text-danger ");
            $('#fax_input').next("div").remove();
            error[11] = 0;
         }
     
         if ($("#i_password").val().trim() == "")
         {
            $('#i_password').parent().addClass("has-error text-danger ");
            $('#i_password').after("<div class='error'><b>Password Required </b></div>");
            error[12] = 1;
         }
         else
         {
            $('#i_password').parent().removeClass("has-error text-danger ");
            $('#i_password').next("div").remove();
            error[12] = 0;
         }
     
         if ($("#c_password").val().trim() == "" || $("#c_password").val().trim() != $('#i_password').val().trim())
         {
            $('#c_password').parent().addClass("has-error text-danger ");
            $('#c_password').after("<div class='error'><b>Password mismatch </b></div>");
            error[13] = 1;
         }
         else
         {
            $('#c_password').parent().removeClass("has-error text-danger ");
            $('#c_password').next("div").remove();
            error[13] = 0;
         }

         if ($("#r_city").val().trim() == "" || ! (onlyLetterRegExp.test($("#r_city").val().trim())))
         {
            $('#r_city').parent().addClass("has-error text-danger ");
            $('#r_city').after("<div class='error'><b>Residential city required & alphabets only </b></div>");
            error[14] = 1;
         }
         else
         {
            $('#r_city').parent().removeClass("has-error text-danger ");
            $('#r_city').next("div").remove();
            error[15] = 0;
         }
     
         if ($("#state_input").val().trim() == "" || ! (onlyLetterRegExp.test($("#state_input").val().trim())))
         {
            $('#state_input').parent().addClass("has-error text-danger ");
            $('#state_input').after("<div class='error'><b>Residential state required & alphabets only </b></div>");
            error[16] = 1;
         }
         else
         {
            $('#state_input').parent().removeClass("has-error text-danger ");
            $('#state_input').next("div").remove();
            error[16] = 0;
         }
     
         if ($("#r_zip_code").val().trim() == "" || ! (onlyNumberRegExp.test($("#r_zip_code").val().trim())))
         {
            $('#r_zip_code').parent().addClass("has-error text-danger ");
            $('#r_zip_code').after("<div class='error'><b>Residential zip code required & numeric only </b></div>");
            error[17] = 1;
         }
         else
         {
            $('#r_zip_code').parent().removeClass("has-error text-danger ");
            $('#r_zip_code').next("div").remove();
            error[17] = 0;
         }
     
         if ($("#title_input").val().trim() == "" || ! (onlyLetterRegExp.test($("#title_input").val().trim())))
         {
            $('#title_input').parent().addClass("has-error text-danger ");
            $('#title_input').after("<div class='error'><b>Designation required & alphabets only </b></div>");
            error[18] = 1;
         }
         else
         {
            $('#title_input').parent().removeClass("has-error text-danger ");
            $('#title_input').next("div").remove();
            error[18] = 0;
         }
     
         if ($("#company").val().trim() == "" || ! (onlyLetterRegExp.test($("#company").val().trim())))
         {
            $('#company').parent().addClass("has-error text-danger ");
            $('#company').after("<div class='error'><b>Company name required & alphabets only </b></div>");
            error[19] = 1;
         }
         else
         {
            $('#company').parent().removeClass("has-error text-danger ");
            $('#company').next("div").remove();
            error[19] = 0;
         }
     
         if ($("#o_city").val().trim() == "" || ! (onlyLetterRegExp.test($("#o_city").val().trim())))
         {
            $('#o_city').parent().addClass("has-error text-danger ");
            $('#o_city').after("<div class='error'><b>Official city required & alphabets only </b></div>");
            error[20] = 1;
         }
         else
         {
            $('#o_city').parent().removeClass("has-error text-danger ");
            $('#o_city').next("div").remove();
            error[20] = 0;
         }
     
         if ($("#o_state").val().trim() == "" || ! (onlyLetterRegExp.test($("#o_state").val().trim())))
         {
            $('#o_state').parent().addClass("has-error text-danger ");
            $('#o_state').after("<div class='error'><b>Official state required & alphabets only </b></div>");
            error[21] = 1;
         }
         else
         {
            $('#o_state').parent().removeClass("has-error text-danger ");
            $('#o_state').next("div").remove();
            error[21] = 0;
         }
     
         if ($("#o_zip_code").val().trim() == "" || ! (onlyNumberRegExp.test($("#o_zip_code").val().trim())))
         {
            $('#o_zip_code').parent().addClass("has-error text-danger ");
            $('#o_zip_code').after("<div class='error'><b>Official zip code required & numeric only </b></div>");
            error[22] = 1;
         }
         else
         {
            $('#o_zip_code').parent().removeClass("has-error text-danger ");
            $('#o_zip_code').next("div").remove();
            error[22] = 0;
         }

         if ( ! ($("#Call").is(':checked')) && ! ($("#SMS").is(':checked')) && ! ($("#Email").is(':checked')))
         {
             $('#communicate_through').parent().addClass("has-error text-danger ");
             $('#submit').before("<div class='error text-danger'><b>Select one Communication medium</b></div>");
             error[23] = 1;
         }
         else
         {
             $('#communicate_through').parent().removeClass("has-error text-danger ");
             $('#communicate_through').next("div").remove();
             error[23] = 0;
         }

         if (error.indexOf(1) != -1)
         {
            return false;
         }
     });
 });


$(document).ready(function() {
    $('#data').on('click', 'tr', function() {
		var id_request = $(this).attr('id');
	    if(navigator.onLine == true) {
            $("#update").data("ids", id_request);
            $("#delete").data("ids", id_request);
            $("#stack").data("ids", id_request);
            $.ajax({
                url: "http://" + SERVER_URL + "/php/Registration_DB/modal_data.php",
                type: "POST",
                dataType: "json",
                data: {id: id_request},
                success: function (response) {
                    $("#modal").modal("show");
                    $("#modal .modal-body ").html("<div><b>Name : </b>" + response.name + "</div>" +
                        "<div><b>Date of birth : </b>" + response.date_of_birth + "</div>" +
                        "<div><b>Residential Address : </b>" + response.residential_address + "</div>" +
                        "<div><b>Office Address : </b>" + response.official_address + "</div>" +
                        "<div><b>Telephone : </b>" + response.telephone + "</div>" +
                        "<div><b>Mobile : </b>" + response.mobile + "</div>" +
                        "<div><b>Email : </b>" + response.email + "</div>" +
                        "<div><b>Password : </b>" + response.password + "</div>" +
                        "<div><b>Designation : </b>" + response.designation + "</div>" +
                        "<div><b>Company : </b>" + response.company + "</div>" +
                        "<div><b>Stack overflow id : </b>" + response.stackoverflow_id + "</div>");

                }
            });
            $("#update").show();
            $("#delete").show();
            $("#stack").show();
        }
        else {
            var id_request = $(this).attr('id');
            $("#modal").modal("show");
            $("#modal .modal-body ").html("<div><b>Name : </b>" + $("#" + id_request + " td").eq(0).text() + "</div>" +
                                          "<div><b>Residential Address : </b>" + $("#" + id_request + " td").eq(1).text() + "</div>" +
                                          "<div><b>Telephone : </b>" + $("#" + id_request + " td").eq(2).text() + "</div>" +
                                          "<div><b>Mobile : </b>" + $("#" + id_request + " td").eq(3).text() + "</div>" +
                                          "<div><b>Email : </b>" + $("#" + id_request + " td").eq(4).text() + "</div>" +
                                          "<div><b>Designation : </b>" + $("#" + id_request + " td").eq(5).text() + "</div>" +
                                          "<div><b>Comapny : </b>" + $("#" + id_request + " td").eq(6).text() + "</div>" +
                                          "<div><b>Official Address : </b>" + $("#" + id_request + " td").eq(7).text() + "</div>" +
                                          "<div><b>Stack Overflow id : </b>" + $("#" + id_request + " td").eq(8).text() + "</div>");
            $("#update").hide();
            $("#delete").hide();
            $("#stack").hide();
        }
	});
});



$("#modal_update").ready(function() {
    $("#update").click(function() {
        var id_request = $(this).data('ids');
        $("#update_db").data("ids",id_request);
        $.ajax( {
            url  : "http://" + SERVER_URL + "/php/Registration_DB/modal_data_update.php",
            type : "POST",
            dataType : "json",
            data : { id : id_request },
            success : function (response) {
                $("#modal").modal("hide")
                $("#modal_update").modal("show");
                $("#modal-body-update").attr("height",$("#modal-body-update").height($(window).height()-250));
                $("#modal_update .modal-body").html(
                "<div><input type='hidden' class='form-control' name='update_id' id='update_id' value='"+response.PK_id+"' ></div>"+
                "<div class='form-group'><b>Prefix : </b><input type='text' class='form-control' name='update_prefix' id='update_prefix' value='"+response.prefix+"' disabled='true'></div>"+
                "<div class='form-group'><b>First Name : </b><input type='text' class='form-control' name='update_first_name' id='update_first_name' value='"+response.first_name+"' disabled='true'></div>"+
                "<div class='form-group'><b>Middle Name : </b><input type='text' class='form-control' name='update_middle_name' id='update_middle_name' value='"+response.middle_name+"' disabled='true'></div>"+
                "<div class='form-group'><b>Last Name : </b><input type='text' class='form-control' name='update_last_name' id='update_last_name' value='"+response.last_name+"' disabled='true'></div>"+
                "<div class='form-group'><b>Date of birth : </b><input type='date' class='form-control' name='update_dob' id='update_dob' value='"+response.date_of_birth+"'></div>"+
                "<div class='form-group'><b>Residential Street : </b><input type='text' class='form-control' name='update_r_street' id='update_r_street' value='"+response.r_street+"'></div>"+
                "<div class='form-group'><b>Residential City : </b><input type='text' class='form-control' name='update_r_city' id='update_r_city' value='"+response.r_city+"'></div>"+
                "<div class='form-group'><b>Residential State : </b><input type='text' class='form-control' name='update_r_state' id='update_r_state' value='"+response.r_state+"'></div>"+
                "<div class='form-group'><b>Residential Zip Code : </b><input type='text' class='form-control' name='update_r_zip_code' id='update_r_zip_code' value='"+response.r_zip+"'></div>"+
                "<div class='form-group'><b>Official Street : </b><input type='text' class='form-control' name='update_o_street' id='update_o_street' value='"+response.o_street+"'></div>"+
                "<div class='form-group'><b>Official City : </b><input type='text' class='form-control' name='update_o_city' id='update_o_city' value='"+response.o_city+"'></div>"+
                "<div class='form-group'><b>Official State : </b><input type='text' class='form-control' name='update_o_state' id='update_o_state' value='"+response.o_state+"'></div>"+
                "<div class='form-group'><b>Official Zip Code : </b><input type='text' class='form-control' name='update_o_zip_code' id='update_o_zip_code' value='"+response.o_zip+"'></div>"+
                "<div class='form-group'><b>Telephone : </b><input type='text' class='form-control' name='update_telephone' id='update_telephone' value='"+response.telephone+"'></div>"+
                "<div class='form-group'><b>Mobile : </b><input type='text' class='form-control' name='update_mobile' id='update_mobile' value='"+response.mobile+"'></div>"+
                "<div class='form-group'><b>Email : </b><input type='text' class='form-control' name='update_email' id='update_email' value='"+response.email+"' disabled></div>"+
                "<div class='form-group'><b>Password : </b><input type='text' class='form-control' name='update_password' id='update_password' value='"+response.password+"'></div>"+
                "<div class='form-group'><b>Designation : </b><input type='text' class='form-control' name='update_designation' id='update_designation' value='"+response.designation+"'></div>"+
                "<div class='form-group'><b>Company : </b><input type='text' class='form-control' name='update_company' id='update_company' value='"+response.company+"'></div>"+
                "<div class='form-group'><b>Stack Overflow id : </b><input type='text' class='form-control' name='update_stackoverflow_id' id='update_stackoverflow_id' value='"+response.stackoverflow_id+"'></div>");
            }
        });
    });
});

$("#modal_update").ready(function() {
    $("#update_db").click(function() {
        $(".text-danger").remove();
        var id_request = $(this).data('ids');
        var error = false;

        if ($("#update_r_street").val().trim() == "")
        {
            $("#update_r_street").after("<div class='text-danger'>Required Field</div>");
            error = true;
        }

        if ($("#update_r_city").val().trim() == "" || ! (onlyLetterRegExp.test($("#update_r_city").val().trim())))
        {
            $("#update_r_city").after("<div class='text-danger'>It is a required field and only alphabets allowed</div>");
            error = true;
        }

        if ($("#update_r_state").val().trim() == "" || ! (onlyLetterRegExp.test($("#update_r_state").val().trim())))
        {
            $("#update_r_state").after("<div class='text-danger'>It is a required field and only alphabets allowed</div>");
            error = true;
        }

        if ($("#update_r_zip_code").val().trim() == "" || ! (onlyNumberRegExp.test($("#update_r_zip_code").val().trim())))
        {
            error = true;
            $("#update_r_zip_code").after("<div class='text-danger'>It is a required field and only numbers allowed</div>");
        }

        if ($("#update_o_street").val().trim() == "")
        {
            error = true;
            $("#update_o_street").after("<div class='text-danger'>Required Field</div>");
        }

        if ($("#update_o_state").val().trim() == "" || ! (onlyLetterRegExp.test($("#update_o_state").val().trim())))
        {
            error = true;
            $("#update_o_state").after("<div class='text-danger'>It is a required field and only alphabets allowed</div>");
        }

        if ($("#update_o_city").val().trim() == "" || ! (onlyLetterRegExp.test($("#update_o_city").val().trim())))
        {
            error = true;
            $("#update_o_city").after("<div class='text-danger'>It is a required field and only alphabets allowed</div>");
        }

        if ($("#update_o_zip_code").val().trim() == "" || ! (onlyNumberRegExp.test($("#update_o_zip_code").val().trim())))
        {
            error = true;
            $("#update_o_zip_code").after("<div class='text-danger'>It is a required field and only numbers allowed</div>");
        }

        if ($("#update_password").val().trim() == "" )
        {
            error = true;
            $("#update_password").after("<div class='text-danger'>It is a required field</div>");
        }

        if ($("#update_telephone").val().trim() == "" || ! (onlyNumberRegExp.test($("#update_telephone").val().trim())))
        {
            error = true;
            $("#update_telephone").after("<div class='text-danger'>It is a required field and only numbers allowed</div>");
        }

        if ($("#update_mobile").val().trim() == "" || ! (onlyNumberRegExp.test($("#update_mobile").val().trim())))
        {
            error = true;
            $("#update_mobile").after("<div class='text-danger'>It is a required field and only numbers allowed</div>");
        }

        if ($("#update_designation").val().trim() == "" || ! (onlyLetterRegExp.test($("#update_designation").val().trim())))
        {
            error = true;
            $("#update_designation").after("<div class='text-danger'>It is a required field and only alphabets allowed</div>");
        }

        if ($("#update_company").val().trim() == "" || ! (onlyLetterRegExp.test($("#update_company").val().trim())))
        {
            error = true;
            $("#update_company").after("<div class='text-danger'>It is a required field and only alphabets allowed</div>");
        }

        if ($("#update_stackoverflow_id").val().trim() == "" || ! (onlyNumberRegExp.test($("#update_stackoverflow_id").val().trim())))
        {
            error = true;
            $("#update_company").after("<div class='text-danger'>It is a required field and only alphabets allowed</div>");
        }

        if( ! error)
        {
            $.ajax( {
                url  : "http://" + SERVER_URL + "/php/Registration_DB/update_db.php",
                type : "POST",
                dataType : "json",
                data : $("form").serialize()
            });
            $("#modal_update").modal("hide");
            $("#" + id_request).html("" +
            "<td>" + $("#update_prefix").val() + " " + $("#update_first_name").val() + " " + $("#update_middle_name").val() + " " + $("#update_last_name").val() + "</td>"+
            "<td>" + $("#update_r_street").val() + " " + $("#update_r_city").val() + " " + $("#update_r_state").val() + " " + $("#update_r_zip_code").val() + "</td>" +
            "<td>" + $("#update_telephone").val() + "</td>" +
            "<td>" + $("#update_mobile").val() + "</td>" +
            "<td>" + $("#update_email").val() + "</td>" +
            "<td>" + $("#update_designation").val() + "</td>" +
            "<td>" + $("#update_company").val() + "</td>" +
            "<td>" + $("#update_o_street").val() + " " + $("#update_o_city").val() + " " + $("#update_o_state").val() + " " + $("#update_o_zip_code").val() + "</td>" +
            "<td>" + $("#update_stackoverflow_id").val() + "</td>");
        }
        return false;
    });
    $("#close_update").click(function() {
        $("#modal_update").modal("hide");
        $("#modal").modal("show");
    });
});

$("#modal_delete").ready(function() {
    $("#delete").click(function() {
        var  id_request = $(this).data('ids');
        $("#delete_db").data("ids",id_request);
        $("#modal_delete .modal-body").html("Are you sure you want to delete it");
        $("#modal").modal("hide");
        $("#modal_delete").modal("show");
    });
});

$("#modal_delete").ready(function() {
    $("#delete_db").click(function() {
        var id_request = $(this).data('ids');
        $.ajax( {
            url  : "http://" + SERVER_URL + "/php/Registration_DB/delete_data.php",
            type : "POST",
            dataType : "json",
            data : { id : id_request }
        });
        $("#"+id_request).remove();
        $("#modal_db").modal("hide");
    });
    $("#close_delete").click(function() {
        $("#modal_delete").modal("hide");
        $("#modal").modal("show");
    });
});

$("#modal_stack").ready(function() {
    $("#stack").click(function() {
        var id_request = $(this).data('ids');
        $("#loader").removeClass("hidden");
        $("#modal .modal-body, #modal .modal-header, #modal .modal-footer").addClass("hidden");
        $.ajax( {
            url  : "http://" + SERVER_URL + "/php/Registration_DB/stack_data.php",
            type : "POST",
            dataType : "json",
            data : { id : id_request },
            timeout : 10000,
            success : function(response) {
                $.ajax({
                    url  : "https://api.stackexchange.com/2.2/users/"+response.stackoverflow_id+"?order=desc&sort=reputation&site=stackoverflow",
                    type : "GET",
                    dataType : "json",
                    timeout : 10000,
                    success : function (response_stack) {
                        $("#modal").modal("hide");
                        $("#modal_stack").modal("show");
                        $("#modal-body-stackoverflow").attr("height",$("#modal-body-stackoverflow").height($(window).height()-250));
                        $("#modal_stack .modal-body ").html("<div class='form-group'><div ><center><img src='"+response_stack.items[0].profile_image+"' " +
                        "class='img-circle img-responsive' alt='No image'></center></div>" +
                        "<div class='col-lg-12'><span><label>Website url:</label></span><span>" +
                        "<input type='text' class='form-control' value='"+response_stack.items[0].website_url+"' " +
                        "disabled></span></div>" +
                        "<div class='col-lg-12'><span><label>Link:</label></span><span>"+
                        "<input type='text' class='form-control' value='"+response_stack.items[0].link+"' disabled></div>" +
                        "<div class='col-lg-12'><span><label>Display Name:</label></span><span>"+
                        "<input type='text' class='form-control' value='"+response_stack.items[0].display_name+"' disabled></div>" +
                        "<div class='col-lg-4'><span><label>Badge Bronze:</label></span><span>"+
                        "<input type='text' class='form-control' value='"+response_stack.items[0].badge_counts.bronze+"' disabled></div>" +
                        "<div class='col-lg-4'><span><label>Badge Silver:</label></span><span>"+
                        "<input type='text' class='form-control' value='"+response_stack.items[0].badge_counts.silver+"' disabled></div>" +
                        "<div class='col-lg-4'><span><label>Badge Gold:</label></span><span>"+
                        "<input type='text' class='form-control' value='"+response_stack.items[0].badge_counts.gold+"' disabled></div>" +
                        "<div class='col-lg-12'><span><label>User Id:</label></span><span>"+
                        "<input type='text' class='form-control' value='"+response_stack.items[0].user_id+"' disabled></div>" +
                        "</div>");
                    },
                    error : function (response) {
                        $("#modal").modal("hide");
                        $("#modal_stack").modal("show");
                        $("#modal-body-stackoverflow").attr("height",500);
                        $("#modal_stack .modal-body ").html("<div class='text-danger alert-danger h1 text-center'>Invalid Id</div>" +
                        "<div>Error Message:"+response.responseJSON.error_message+"</div>");
                    }
                });
            }
        }).fail(function () {
            $("#modal").modal("hide");
            $("#modal_stack").modal("show");
            $("#modal-body-stackoverflow").attr("height",500);
            $("#modal_stack .modal-body ").html("<div class='text-danger alert-danger h1 text-center'>Not found</div>");
        });
    });
    $("#close_stack").click(function() {
        $("#loader").addClass("hidden");
        $("#modal .modal-body, #modal .modal-header, #modal .modal-footer").removeClass("hidden");
        $("#modal_stack").modal("hide");
        $("#modal").modal("show");
    });
});

// $(document).ready(function(){
//     $("#twitterbtn").click(function(){
//         $.ajax({
//             POST /oauth/request_token HTTP/1.1,
//             User-Agent: ishan,
//             Host: api.twitter.com,
//             Accept: ishan,
//             Authorization:
//                 OAuth oauth_callback="http://www.localhost/dashboard.php",
//                 oauth_consumer_key="1FD6vxKagltopsYR1vf1MN7PD",
//         });
//     });
// });


$(document).ready(function(){
    window.fbAsyncInit = function() {
    FB.init({
      appId      : '408384566195405',
      xfbml      : true,
      version    : 'v2.9'
    });
    FB.AppEvents.logPageView();
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
});

$(document).ready(function(){
    $("#facebookbtn").click(function(){
        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                FB.api('/me?fields=name,email',  function(response) {     
                    window.location = "http://localhost/php/Registration_DB/loginfb.php?name="+response.name+"&&email="+response.email;
                },{ scope: 'email' });
            }
            else {
                FB.login(function(){
                    FB.api('/me?fields=name,email', function(response) {     
                        if(response.name != undefined && response.email != undefined)
                            window.location = "http://localhost/php/Registration_DB/loginfb.php?name="+response.name+"&&email="+response.email;
                    });
                },{ scope: 'email' });
            }
        });
    });
});


// if ('serviceWorker' in navigator) {
//     window.addEventListener('load', function() {
//         navigator.serviceWorker.register('service_worker.js').then(function(registration) {
//             // Registration was successful
//             console.log('ServiceWorker registration successful with scope: ', registration.scope);
//         }).catch(function(err) {
//             // registration failed :(
//             console.log('ServiceWorker registration failed: ', err);
//         });
//     });
// }
